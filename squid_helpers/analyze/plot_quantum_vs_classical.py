from pathlib import Path
from typing import List, Union

import numpy as np
import yaml
from matplotlib import pyplot as plt


def check_if_sub_config(filter_config: dict, check_config: dict) -> bool:
    for f_k, f_v in filter_config.items():
        if f_k == "BOOTSTRAP HASH":
            continue

        if f_k not in check_config:
            return False

        if f_v is None:
            continue

        c_v = check_config[f_k]

        if isinstance(f_v, dict):
            if not isinstance(c_v, dict):
                return False
            elif not check_if_sub_config(f_v, c_v):
                return False
        elif isinstance(f_v, list):
            if not isinstance(c_v, list):
                return False
            if len(c_v) != len(f_v):
                return False
            for c_l_v, f_l_v in zip(c_v, f_v):
                if c_l_v != f_l_v:
                    return False
        elif c_v != f_v:
            return False

    return True


def get_logs(
    start_date: str = None,
    end_date: str = None,
    look_in_folder: str = None,
    filter_config: dict = None,
):
    if look_in_folder is None:
        look_in_folder = "log_bootstrap"

    for path in Path(look_in_folder).rglob("config.yml"):
        # If before start date, ignore.
        if start_date is not None:
            if str(path) < f"{look_in_folder}/{start_date}":
                continue

        # If after end date, ignore.
        if end_date is not None and str(path) > end_date:
            if str(path) > f"{look_in_folder}/{end_date}":
                continue

        # Assert run is successful
        if not (path.parent / "train_accuracies.npy").exists():
            continue

        # Make sure the config matches
        if filter_config is not None:
            with open(path, mode="r") as f:
                config = yaml.load(f)
            if not check_if_sub_config(filter_config, config):
                continue

        yield path


def aggregate_configs(config_paths: List[Union[Path, str]]):
    train_losses = []
    train_accs = []
    val_losses = []
    val_accs = []

    for config_path in config_paths:
        folder_path = Path(config_path).parent
        train_loss = np.load(folder_path / "train_losses.npy")
        train_acc = np.load(folder_path / "train_accuracies.npy")
        val_loss = np.load(folder_path / "val_losses.npy")
        val_acc = np.load(folder_path / "val_accuracies.npy")

        train_accs.append(train_acc)
        train_losses.append(train_loss)
        val_accs.append(val_acc)
        val_losses.append(val_loss)

    train_losses = np.array(train_losses)
    train_accs = np.array(train_accs)
    val_losses = np.array(val_losses)
    val_accs = np.array(val_accs)

    return (train_losses, val_losses), (train_accs, val_accs)


def plot_compare_bootstrap_runs():
    classical_folder_path = Path("log_bootstrap/2020-05-18_20:08:51/config.yml").parent
    quant_folder_path = Path("log_bootstrap/2020-05-18_20:11:09/config.yml").parent

    class_train_losses = np.load(classical_folder_path / "train_losses.npy")
    class_train_accs = np.load(classical_folder_path / "train_accuracies.npy")
    class_val_losses = np.load(classical_folder_path / "val_losses.npy")
    class_val_accs = np.load(classical_folder_path / "val_accuracies.npy")

    quant_train_losses = np.load(quant_folder_path / "train_losses.npy")
    quant_train_accs = np.load(quant_folder_path / "train_accuracies.npy")
    quant_val_losses = np.load(quant_folder_path / "val_losses.npy")
    quant_val_accs = np.load(quant_folder_path / "val_accuracies.npy")

    epochs = list(range(quant_train_losses.shape[1]))

    # Medians, Percentiles etc.
    class_low_train_loss = np.percentile(class_train_losses, 25, axis=0)
    class_med_train_loss = np.percentile(class_train_losses, 50, axis=0)
    class_high_train_loss = np.percentile(class_train_losses, 75, axis=0)
    class_low_train_acc = np.percentile(class_train_accs, 25, axis=0)
    class_med_train_acc = np.percentile(class_train_accs, 50, axis=0)
    class_high_train_acc = np.percentile(class_train_accs, 75, axis=0)

    class_low_val_loss = np.percentile(class_val_losses, 25, axis=0)
    class_med_val_loss = np.percentile(class_val_losses, 50, axis=0)
    class_high_val_loss = np.percentile(class_val_losses, 75, axis=0)
    class_low_val_acc = np.percentile(class_val_accs, 25, axis=0)
    class_med_val_acc = np.percentile(class_val_accs, 50, axis=0)
    class_high_val_acc = np.percentile(class_val_accs, 75, axis=0)

    quant_low_train_loss = np.percentile(quant_train_losses, 25, axis=0)
    quant_med_train_loss = np.percentile(quant_train_losses, 50, axis=0)
    quant_high_train_loss = np.percentile(quant_train_losses, 75, axis=0)
    quant_low_train_acc = np.percentile(quant_train_accs, 25, axis=0)
    quant_med_train_acc = np.percentile(quant_train_accs, 50, axis=0)
    quant_high_train_acc = np.percentile(quant_train_accs, 75, axis=0)

    quant_low_val_loss = np.percentile(quant_val_losses, 25, axis=0)
    quant_med_val_loss = np.percentile(quant_val_losses, 50, axis=0)
    quant_high_val_loss = np.percentile(quant_val_losses, 75, axis=0)
    quant_low_val_acc = np.percentile(quant_val_accs, 25, axis=0)
    quant_med_val_acc = np.percentile(quant_val_accs, 50, axis=0)
    quant_high_val_acc = np.percentile(quant_val_accs, 75, axis=0)

    # Do plotting
    plt.plot(epochs, class_med_train_loss, label="Train Classical", color="m")
    plt.plot(epochs, class_med_val_loss, label="Validation Classical", color="y")
    plt.fill_between(
        epochs, class_low_train_loss, class_high_train_loss, color="m", alpha=0.5
    )
    plt.fill_between(
        epochs, class_low_val_loss, class_high_val_loss, color="y", alpha=0.5
    )
    plt.plot(epochs, quant_med_train_loss, label="Train Quantum", color="g")
    plt.plot(epochs, quant_med_val_loss, label="Validation Quantum", color="orange")
    plt.fill_between(
        epochs, quant_low_train_loss, quant_high_train_loss, color="g", alpha=0.5
    )
    plt.fill_between(
        epochs, quant_low_val_loss, quant_high_val_loss, color="orange", alpha=0.5
    )
    plt.title("Loss IQR (on Higgs vs QCD pT 250-500 GeV)")
    plt.xlabel("Epochs")
    plt.ylabel("CE Loss")
    plt.legend()
    plt.savefig("plots/higgs_vs_qcd_pt_250_500_loss.png", bbox_inches="tight")
    plt.cla()
    plt.clf()

    plt.plot(epochs, class_med_train_acc, label="Train Classical", color="m")
    plt.plot(epochs, class_med_val_acc, label="Validation Classical", color="y")
    plt.fill_between(
        epochs, class_low_train_acc, class_high_train_acc, color="m", alpha=0.5
    )
    plt.fill_between(
        epochs, class_low_val_acc, class_high_val_acc, color="y", alpha=0.5
    )
    plt.plot(epochs, quant_med_train_acc, label="Train Quantum", color="g")
    plt.plot(epochs, quant_med_val_acc, label="Validation Quantum", color="orange")
    plt.fill_between(
        epochs, quant_low_train_acc, quant_high_train_acc, color="g", alpha=0.5
    )
    plt.fill_between(
        epochs, quant_low_val_acc, quant_high_val_acc, color="orange", alpha=0.5
    )
    plt.title("Accuracy IQR (on Higgs vs QCD pT 250-500 GeV)")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig("plots/higgs_vs_qcd_pt_250_500_acc.png", bbox_inches="tight")
    plt.cla()
    plt.clf()


def plot_x_to_y():
    class_to_quant_folder_path = Path(
        "log/classical_to_quantum_2nd_attempt/config.yml"
    ).parent
    quant_to_class_folder_path = Path(
        "log/quantum_to_classical_2nd_attempt/config.yml"
    ).parent

    class_to_quant_train_losses = np.load(
        class_to_quant_folder_path / "train_losses.npy"
    )
    class_to_quant_train_accs = np.load(
        class_to_quant_folder_path / "train_accuracies.npy"
    )
    class_to_quant_val_losses = np.load(class_to_quant_folder_path / "val_losses.npy")
    class_to_quant_val_accs = np.load(class_to_quant_folder_path / "val_accuracies.npy")

    quant_to_class_train_losses = np.load(
        quant_to_class_folder_path / "train_losses.npy"
    )
    quant_to_class_train_accs = np.load(
        quant_to_class_folder_path / "train_accuracies.npy"
    )
    quant_to_class_val_losses = np.load(quant_to_class_folder_path / "val_losses.npy")
    quant_to_class_val_accs = np.load(quant_to_class_folder_path / "val_accuracies.npy")

    epochs = list(range(quant_to_class_train_losses.shape[1]))

    # Medians, Percentiles etc.
    class_to_quant_low_train_loss = np.percentile(
        class_to_quant_train_losses, 25, axis=0
    )
    class_to_quant_med_train_loss = np.percentile(
        class_to_quant_train_losses, 50, axis=0
    )
    class_to_quant_high_train_loss = np.percentile(
        class_to_quant_train_losses, 75, axis=0
    )
    class_to_quant_low_train_acc = np.percentile(class_to_quant_train_accs, 25, axis=0)
    class_to_quant_med_train_acc = np.percentile(class_to_quant_train_accs, 50, axis=0)
    class_to_quant_high_train_acc = np.percentile(class_to_quant_train_accs, 75, axis=0)

    class_to_quant_med_val_loss = np.percentile(class_to_quant_val_losses, 50, axis=0)
    class_to_quant_low_val_acc = np.percentile(class_to_quant_val_accs, 25, axis=0)
    class_to_quant_med_val_acc = np.percentile(class_to_quant_val_accs, 50, axis=0)
    class_to_quant_high_val_acc = np.percentile(class_to_quant_val_accs, 75, axis=0)

    quant_to_class_low_train_loss = np.percentile(
        quant_to_class_train_losses, 25, axis=0
    )
    quant_to_class_med_train_loss = np.percentile(
        quant_to_class_train_losses, 50, axis=0
    )
    quant_to_class_high_train_loss = np.percentile(
        quant_to_class_train_losses, 75, axis=0
    )
    quant_to_class_low_train_acc = np.percentile(quant_to_class_train_accs, 25, axis=0)
    quant_to_class_med_train_acc = np.percentile(quant_to_class_train_accs, 50, axis=0)
    quant_to_class_high_train_acc = np.percentile(quant_to_class_train_accs, 75, axis=0)

    quant_to_class_low_val_loss = np.percentile(quant_to_class_val_losses, 25, axis=0)
    quant_to_class_med_val_loss = np.percentile(quant_to_class_val_losses, 50, axis=0)
    quant_to_class_high_val_loss = np.percentile(quant_to_class_val_losses, 75, axis=0)
    quant_to_class_low_val_acc = np.percentile(quant_to_class_val_accs, 25, axis=0)
    quant_to_class_med_val_acc = np.percentile(quant_to_class_val_accs, 50, axis=0)
    quant_to_class_high_val_acc = np.percentile(quant_to_class_val_accs, 75, axis=0)

    # Do plotting
    plt.plot(epochs, class_to_quant_med_train_loss, label="Train Classical", color="m")
    plt.plot(
        epochs, class_to_quant_med_val_loss, label="Validation Classical", color="y"
    )
    plt.fill_between(
        epochs,
        class_to_quant_low_train_loss,
        class_to_quant_high_train_loss,
        color="m",
        alpha=0.5,
    )
    plt.fill_between(
        epochs,
        quant_to_class_low_val_loss,
        quant_to_class_high_val_loss,
        color="y",
        alpha=0.5,
    )
    plt.plot(epochs, quant_to_class_med_train_loss, label="Train Quantum", color="g")
    plt.plot(
        epochs, quant_to_class_med_val_loss, label="Validation Quantum", color="orange"
    )
    plt.fill_between(
        epochs,
        quant_to_class_low_train_loss,
        quant_to_class_high_train_loss,
        color="g",
        alpha=0.5,
    )
    plt.fill_between(
        epochs,
        quant_to_class_low_val_loss,
        quant_to_class_high_val_loss,
        color="orange",
        alpha=0.5,
    )
    plt.title("Loss IQR (on Higgs vs QCD pT 250-500 GeV) Start from the other encoding")
    plt.xlabel("Epochs")
    plt.ylabel("CE Loss")
    plt.legend()
    plt.savefig(
        "plots/higgs_vs_qcd_start_other_pt_250_500_loss.png", bbox_inches="tight"
    )
    plt.cla()
    plt.clf()

    plt.plot(epochs, class_to_quant_med_train_acc, label="Train Classical", color="m")
    plt.plot(
        epochs, class_to_quant_med_val_acc, label="Validation Classical", color="y"
    )
    plt.fill_between(
        epochs,
        class_to_quant_low_train_acc,
        class_to_quant_high_train_acc,
        color="m",
        alpha=0.5,
    )
    plt.fill_between(
        epochs,
        class_to_quant_low_val_acc,
        class_to_quant_high_val_acc,
        color="y",
        alpha=0.5,
    )
    plt.plot(epochs, quant_to_class_med_train_acc, label="Train Quantum", color="g")
    plt.plot(
        epochs, quant_to_class_med_val_acc, label="Validation Quantum", color="orange"
    )
    plt.fill_between(
        epochs,
        quant_to_class_low_train_acc,
        quant_to_class_high_train_acc,
        color="g",
        alpha=0.5,
    )
    plt.fill_between(
        epochs,
        quant_to_class_low_val_acc,
        quant_to_class_high_val_acc,
        color="orange",
        alpha=0.5,
    )
    plt.title(
        "Accuracy IQR (on Higgs vs QCD pT 250-500 GeV) Start from the other encoding"
    )
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig(
        "plots/higgs_vs_qcd_start_other_pt_250_500_acc.png", bbox_inches="tight"
    )
    plt.cla()
    plt.clf()
    pass


def main():

    classical_folder_path = Path("log_bootstrap/2020-05-21_15:55:30/config.yml").parent

    # Get the quantum configs
    with open("log_bootstrap/2020-05-25_10:54:55/config.yml", mode="r") as f:
        config = yaml.load(f)

    quantum_configs = list(
        get_logs("2020-05-19", look_in_folder="log", filter_config=config)
    )

    print(len(quantum_configs))
    exit(0)

    (
        (quant_train_losses, quant_val_losses),
        (quant_train_accs, quant_val_accs),
    ) = aggregate_configs(quantum_configs)

    class_train_losses = np.load(classical_folder_path / "train_losses.npy")
    class_train_accs = np.load(classical_folder_path / "train_accuracies.npy")
    class_val_losses = np.load(classical_folder_path / "val_losses.npy")
    class_val_accs = np.load(classical_folder_path / "val_accuracies.npy")

    epochs = list(range(quant_train_losses.shape[1]))

    # Medians, Percentiles etc.
    class_low_train_loss = np.percentile(class_train_losses, 25, axis=0)
    class_med_train_loss = np.percentile(class_train_losses, 50, axis=0)
    class_high_train_loss = np.percentile(class_train_losses, 75, axis=0)
    class_low_train_acc = np.percentile(class_train_accs, 25, axis=0)
    class_med_train_acc = np.percentile(class_train_accs, 50, axis=0)
    class_high_train_acc = np.percentile(class_train_accs, 75, axis=0)

    class_low_val_loss = np.percentile(class_val_losses, 25, axis=0)
    class_med_val_loss = np.percentile(class_val_losses, 50, axis=0)
    class_high_val_loss = np.percentile(class_val_losses, 75, axis=0)
    class_low_val_acc = np.percentile(class_val_accs, 25, axis=0)
    class_med_val_acc = np.percentile(class_val_accs, 50, axis=0)
    class_high_val_acc = np.percentile(class_val_accs, 75, axis=0)

    quant_low_train_loss = np.percentile(quant_train_losses, 25, axis=0)
    quant_med_train_loss = np.percentile(quant_train_losses, 50, axis=0)
    quant_high_train_loss = np.percentile(quant_train_losses, 75, axis=0)
    quant_low_train_acc = np.percentile(quant_train_accs, 25, axis=0)
    quant_med_train_acc = np.percentile(quant_train_accs, 50, axis=0)
    quant_high_train_acc = np.percentile(quant_train_accs, 75, axis=0)

    quant_low_val_loss = np.percentile(quant_val_losses, 25, axis=0)
    quant_med_val_loss = np.percentile(quant_val_losses, 50, axis=0)
    quant_high_val_loss = np.percentile(quant_val_losses, 75, axis=0)
    quant_low_val_acc = np.percentile(quant_val_accs, 25, axis=0)
    quant_med_val_acc = np.percentile(quant_val_accs, 50, axis=0)
    quant_high_val_acc = np.percentile(quant_val_accs, 75, axis=0)

    # Do plotting
    plt.plot(epochs, class_med_train_loss, label="Train Classical", color="m")
    plt.plot(epochs, class_med_val_loss, label="Validation Classical", color="y")
    plt.fill_between(
        epochs, class_low_train_loss, class_high_train_loss, color="m", alpha=0.5
    )
    plt.fill_between(
        epochs, class_low_val_loss, class_high_val_loss, color="y", alpha=0.5
    )
    plt.plot(epochs, quant_med_train_loss, label="Train Quantum", color="g")
    plt.plot(epochs, quant_med_val_loss, label="Validation Quantum", color="orange")
    plt.fill_between(
        epochs, quant_low_train_loss, quant_high_train_loss, color="g", alpha=0.5
    )
    plt.fill_between(
        epochs, quant_low_val_loss, quant_high_val_loss, color="orange", alpha=0.5
    )
    plt.title("Loss IQR (on Higgs vs QCD pT 250-500 GeV)")
    plt.xlabel("Epochs")
    plt.ylabel("CE Loss")
    plt.legend()
    plt.savefig("plots/higgs_vs_qcd_pt_250_500_big_loss.png", bbox_inches="tight")
    plt.cla()
    plt.clf()

    plt.plot(epochs, class_med_train_acc, label="Train Classical", color="m")
    plt.plot(epochs, class_med_val_acc, label="Validation Classical", color="y")
    plt.fill_between(
        epochs, class_low_train_acc, class_high_train_acc, color="m", alpha=0.5
    )
    plt.fill_between(
        epochs, class_low_val_acc, class_high_val_acc, color="y", alpha=0.5
    )
    plt.plot(epochs, quant_med_train_acc, label="Train Quantum", color="g")
    plt.plot(epochs, quant_med_val_acc, label="Validation Quantum", color="orange")
    plt.fill_between(
        epochs, quant_low_train_acc, quant_high_train_acc, color="g", alpha=0.5
    )
    plt.fill_between(
        epochs, quant_low_val_acc, quant_high_val_acc, color="orange", alpha=0.5
    )
    plt.title("Accuracy IQR (on Higgs vs QCD pT 250-500 GeV)")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig("plots/higgs_vs_qcd_pt_250_500_big_acc.png", bbox_inches="tight")
    plt.cla()
    plt.clf()


if __name__ == "__main__":
    main()
