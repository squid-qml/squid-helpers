import os
import pickle
import sys
import warnings
from collections import defaultdict
from pathlib import Path
from typing import DefaultDict, Dict, List, Tuple

import numpy as np
import torch
import yaml

sys.path.append("src/")
thismodule = sys.modules[__name__]

OUT_FOLDER = "analyze_output_weights"


def look_at_last_weights():
    one_hot_logs = [
        "log/2020-07-19_14:52:11.989511",
        "log/2020-07-19_14:52:12.027700",
        "log/2020-07-19_14:52:12.043114",
        "log/2020-07-19_14:52:12.048313",
        "log/2020-07-19_14:52:12.059509",
        "log/2020-07-19_14:52:12.136138",
        "log/2020-07-19_14:52:12.178438",
        "log/2020-07-19_14:52:12.200003",
        "log/2020-07-19_14:52:12.210413",
        "log/2020-07-19_14:52:12.242279",
        "log/2020-07-19_14:55:18.737584",
        "log/2020-07-19_14:55:21.599885",
        "log/2020-07-19_14:55:22.357501",
        "log/2020-07-19_14:55:23.477296",
        "log/2020-07-19_14:55:23.900297",
        "log/2020-07-19_14:55:24.909408",
        "log/2020-07-19_14:55:25.280502",
        "log/2020-07-19_14:55:25.645216",
        "log/2020-07-19_14:55:26.237526",
        "log/2020-07-19_14:55:27.701619",
        "log/2020-07-19_14:58:25.858771",
        "log/2020-07-19_14:58:31.190713",
        "log/2020-07-19_14:58:31.856992",
        "log/2020-07-19_14:58:34.712041",
        "log/2020-07-19_14:58:35.656112",
        "log/2020-07-19_14:58:37.880096",
        "log/2020-07-19_14:58:38.364354",
        "log/2020-07-19_14:58:39.212542",
        "log/2020-07-19_14:58:39.850403",
        "log/2020-07-19_14:58:43.411685",
        "log/2020-07-19_15:01:33.411756",
        "log/2020-07-19_15:01:40.860404",
        "log/2020-07-19_15:01:42.024852",
        "log/2020-07-19_15:01:46.122676",
        "log/2020-07-19_15:01:47.308231",
        "log/2020-07-19_15:01:50.839569",
        "log/2020-07-19_15:01:51.625903",
        "log/2020-07-19_15:01:53.516938",
        "log/2020-07-19_15:01:53.710107",
        "log/2020-07-19_15:01:59.031933",
        "log/2020-07-19_15:04:41.405333",
        "log/2020-07-19_15:04:51.018969",
        "log/2020-07-19_15:04:52.136182",
        "log/2020-07-19_15:04:58.099453",
        "log/2020-07-19_15:04:59.126372",
        "log/2020-07-19_15:05:04.451836",
        "log/2020-07-19_15:05:05.648601",
        "log/2020-07-19_15:05:07.249720",
        "log/2020-07-19_15:05:07.519922",
        "log/2020-07-19_15:05:14.926163",
        "log/2020-07-19_15:07:48.949652",
        "log/2020-07-19_15:08:01.379766",
        "log/2020-07-19_15:08:02.770187",
        "log/2020-07-19_15:08:09.758804",
        "log/2020-07-19_15:08:11.496334",
        "log/2020-07-19_15:08:18.297229",
        "log/2020-07-19_15:08:19.100530",
        "log/2020-07-19_15:08:20.582495",
        "log/2020-07-19_15:08:21.586522",
        "log/2020-07-19_15:08:31.222637",
        "log/2020-07-19_15:10:57.160177",
        "log/2020-07-19_15:11:12.389142",
        "log/2020-07-19_15:11:14.532304",
        "log/2020-07-19_15:11:22.453477",
        "log/2020-07-19_15:11:24.819591",
        "log/2020-07-19_15:11:33.459164",
        "log/2020-07-19_15:11:34.676814",
        "log/2020-07-19_15:11:35.198427",
        "log/2020-07-19_15:11:36.481506",
        "log/2020-07-19_15:11:48.367450",
        "log/2020-07-19_15:14:06.434176",
        "log/2020-07-19_15:14:24.709220",
        "log/2020-07-19_15:14:26.535228",
        "log/2020-07-19_15:14:35.272225",
        "log/2020-07-19_15:14:38.602725",
        "log/2020-07-19_15:14:48.804359",
        "log/2020-07-19_15:14:50.321555",
        "log/2020-07-19_15:14:50.345803",
        "log/2020-07-19_15:14:52.015655",
        "log/2020-07-19_15:15:05.509652",
        "log/2020-07-19_15:17:16.504270",
        "log/2020-07-19_15:17:37.260069",
        "log/2020-07-19_15:17:39.079195",
        "log/2020-07-19_15:17:49.566727",
        "log/2020-07-19_15:17:53.264041",
        "log/2020-07-19_15:18:04.435742",
        "log/2020-07-19_15:18:06.001300",
        "log/2020-07-19_15:18:06.801272",
        "log/2020-07-19_15:18:08.156916",
        "log/2020-07-19_15:18:23.818190",
        "log/2020-07-19_15:20:26.647659",
        "log/2020-07-19_15:20:49.801350",
        "log/2020-07-19_15:20:52.122166",
        "log/2020-07-19_15:21:03.812669",
        "log/2020-07-19_15:21:07.829754",
        "log/2020-07-19_15:21:19.964207",
        "log/2020-07-19_15:21:21.430128",
        "log/2020-07-19_15:21:22.435821",
        "log/2020-07-19_15:21:24.431615",
        "log/2020-07-19_15:21:42.279495",
        "log/2020-07-19_15:26:10.221636",
        "log/2020-07-19_15:27:44.373712",
        "log/2020-07-19_15:27:44.579927",
        "log/2020-07-19_15:27:44.639750",
        "log/2020-07-19_15:27:45.180201",
        "log/2020-07-19_15:27:45.250940",
        "log/2020-07-19_15:27:45.671147",
        "log/2020-07-19_15:28:22.013451",
        "log/2020-07-19_15:28:22.415463",
        "log/2020-07-19_15:28:23.577576",
        "log/2020-07-19_15:28:24.843888",
    ]

    for log in one_hot_logs:
        log = Path(log)
        if not (log / "best_model.pth").exists():
            continue

        print(log / "")


def get_hash_maps():
    # Create a map from a hash to a config list (non-bootstrap)
    hash_to_configs: DefaultDict[int, List[Path]] = defaultdict(list)
    for config_path in Path("log/").rglob("config.yml"):
        with open(config_path, mode="r") as config_file:
            config = yaml.safe_load(config_file)

        if not (config_path.parent / "best_model.pth").exists():
            continue

        if "BOOTSTRAP HASH" in config:
            if config["BOOTSTRAP HASH"] not in [41818166, 10911107]:
                continue

            hash_to_configs[config["BOOTSTRAP HASH"]].append(config_path)

    # Create a map from bootstrap config, to runs.
    conflicting_hashes = set()
    bootstrap_to_configs: Dict[Tuple[Dict, List[Path]]] = {}
    for bootstrap_config_path in Path("log_bootstrap/").rglob("config.yml"):
        with open(bootstrap_config_path, mode="r") as bootstrap_config_file:
            bootstrap_config = yaml.safe_load(bootstrap_config_file)

        if "BOOTSTRAP HASH" not in bootstrap_config:
            continue
        bootstrap_hash = bootstrap_config["BOOTSTRAP HASH"]
        if bootstrap_hash not in [41818166, 10911107]:
            continue
        # if bootstrap_config['BOOTSTRAP RUNS'] < 10:
        #     continue
        # elif len(hash_to_configs[bootstrap_hash]) < 10:
        #     continue
        # # Filter out anything non-vqc
        # elif bootstrap_config['model 2']['type'] != 'vqc':
        #     continue

        if bootstrap_hash in conflicting_hashes:
            # If hashes are conflicting remove
            warnings.warn(
                f"Conflicting Hashes. There is probably code bug. Hash: {bootstrap_hash}"
            )
            bootstrap_to_configs.pop(bootstrap_hash, None)
            continue

        bootstrap_to_configs[bootstrap_hash] = (
            bootstrap_config,
            hash_to_configs[bootstrap_hash],
        )
        conflicting_hashes.add(bootstrap_hash)

    return bootstrap_to_configs, hash_to_configs


def in_middle(x):

    # Return elements that are kind of in the middle
    return sum([0.25 < y < 1.0 for y in x])


def main():
    global OUT_FOLDER

    bootstrap_to_configs, _ = get_hash_maps()

    for (
        bootstrap_hash,
        (bootstrap_config, config_paths),
    ) in bootstrap_to_configs.items():
        bootstrap_bias_stats = defaultdict(list)
        bootstrap_weight_stats = defaultdict(list)

        successful = True
        config_path: Path
        for config_path in config_paths:
            best_model = torch.load(config_path.parent / "best_model.pth")

            last_bias = best_model["model3.layers.0.bias"].numpy()
            # Normalize based on magnitude
            last_bias = abs(last_bias)
            last_bias /= sum(last_bias)

            last_weight = best_model["model3.layers.0.weight"].numpy()
            last_weight = abs(last_weight)
            last_weight /= sum(last_weight)

            for stat in ["kurtosis", "skew", "entropy", "var", "in_middle"]:
                try:
                    stat_func = getattr(thismodule, stat)
                    bootstrap_bias_stats[stat].append(stat_func(last_bias))
                    bootstrap_weight_stats[stat].append(stat_func(last_weight))

                except Exception as e:
                    warnings.warn(
                        f"Config Path: {config_path} failed on function {stat} with error {e}"
                    )
                    # successful = False
                    # break

            if not successful:
                break

        if not successful:
            continue

        if not os.path.exists(OUT_FOLDER):
            os.makedirs(OUT_FOLDER)

        with open(f"{OUT_FOLDER}/{bootstrap_hash}_weight_stats.pkl", mode="wb") as f:
            pickle.dump(bootstrap_weight_stats, f)

        with open(f"{OUT_FOLDER}/{bootstrap_hash}_bias_stats.pkl", mode="wb") as f:
            pickle.dump(bootstrap_bias_stats, f)


def plot():
    global OUT_FOLDER

    # _, hash_maps = get_hash_maps()

    interesting_hashes = [3813109, 42635410, 41818166, 10911107]

    for h in interesting_hashes:
        with open(f"{OUT_FOLDER}/{h}_weight_stats.pkl", mode="rb") as f:
            weights = pickle.load(f)
        with open(f"{OUT_FOLDER}/{h}_bias_stats.pkl", mode="rb") as f:
            pass
            # bias = pickle.load(f)

        print(h)
        iqr_weights = {}
        for key, values in weights.items():
            if values:
                iqr_weights[key] = np.percentile(values, (25, 50, 75))
                print(key)
                print(
                    [
                        f"{x:.2f}" if isinstance(x, float) else x
                        for x in iqr_weights[key]
                    ]
                )


if __name__ == "__main__":
    main()
    plot()
