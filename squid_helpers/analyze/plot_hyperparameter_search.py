from pathlib import Path

import numpy as np
import yaml
from matplotlib import pyplot as plt


def __util_flatten_list_of_numpy_arrays(arr: [np.ndarray]):
    tmp = []
    for a in arr:
        tmp.extend(a.flatten())
    return np.array(tmp)


def get_logs(start_date: str = None, end_date: str = None):
    for path in Path("log_bootstrap").rglob("config.yml"):
        # If before start date, ignore.
        if start_date is not None:
            if str(path) < f"log_bootstrap/{start_date}":
                continue
        # If after end date, ignore.
        if end_date is not None and str(path) > end_date:
            if str(path) > f"log_bootstrap/{end_date}":
                continue

        # Assert run is successful
        if not (path.parent / "train_accuracies.npy").exists():
            continue

        yield path


def plot_search(config_path: Path):
    folder_path = Path(config_path).parent

    train_losses = np.load(folder_path / "train_losses.npy")
    train_accs = np.load(folder_path / "train_accuracies.npy")
    val_losses = np.load(folder_path / "val_losses.npy")
    val_accs = np.load(folder_path / "val_accuracies.npy")

    # Get some plot specific values
    with open(config_path, mode="r") as f:
        config = yaml.safe_load(f)

    dataset = config["dataset"]["type"]

    epochs = list(range(train_losses.shape[1]))

    # Medians, Percentiles etc.
    low_train_loss = np.percentile(train_losses, 25, axis=0)
    med_train_loss = np.percentile(train_losses, 50, axis=0)
    high_train_loss = np.percentile(train_losses, 75, axis=0)
    low_train_acc = np.percentile(train_accs, 25, axis=0)
    med_train_acc = np.percentile(train_accs, 50, axis=0)
    high_train_acc = np.percentile(train_accs, 75, axis=0)

    low_val_loss = np.percentile(val_losses, 25, axis=0)
    med_val_loss = np.percentile(val_losses, 50, axis=0)
    high_val_loss = np.percentile(val_losses, 75, axis=0)
    low_val_acc = np.percentile(val_accs, 25, axis=0)
    med_val_acc = np.percentile(val_accs, 50, axis=0)
    high_val_acc = np.percentile(val_accs, 75, axis=0)

    # Do plotting
    plt.plot(epochs, med_train_loss, label="Train", color="g")
    plt.plot(epochs, med_val_loss, label="Validation", color="orange")
    plt.fill_between(epochs, low_train_loss, high_train_loss, color="g", alpha=0.5)
    plt.fill_between(epochs, low_val_loss, high_val_loss, color="orange", alpha=0.5)
    plt.title(f"Loss IQR Dataset: {dataset}")
    plt.xlabel("Epochs")
    plt.ylabel("CE Loss")
    plt.legend()
    plt.savefig(f"plots/{dataset}_loss.png", bbox_inches="tight")
    plt.cla()
    plt.clf()

    plt.plot(epochs, med_train_acc, label="Train", color="g")
    plt.plot(epochs, med_val_acc, label="Validation", color="orange")
    plt.fill_between(epochs, low_train_acc, high_train_acc, color="g", alpha=0.5)
    plt.fill_between(epochs, low_val_acc, high_val_acc, color="orange", alpha=0.5)
    plt.title(f"Accuracy IQR Dataset: {dataset}")
    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.legend()
    plt.savefig(f"plots/{dataset}_acc.png", bbox_inches="tight")
    plt.cla()
    plt.clf()


if __name__ == "__main__":
    for config_path in get_logs(start_date="2020-05-03"):
        print(config_path)
        plot_search(config_path)
