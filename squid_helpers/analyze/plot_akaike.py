import sys
import warnings
from pathlib import Path
from typing import List, Union

import numpy as np
import yaml
from matplotlib import pyplot as plt
from read_config_init import init_from_config_path

warnings.filterwarnings("ignore")
sys.path.append(sys.path[0] + "/..")
print(sys.path)


def check_if_sub_config(filter_config: dict, check_config: dict) -> bool:
    for f_k, f_v in filter_config.items():
        if f_k == "BOOTSTRAP HASH":
            continue

        if f_k not in check_config:
            return False

        if f_v is None:
            continue

        c_v = check_config[f_k]

        if isinstance(f_v, dict):
            if not isinstance(c_v, dict):
                return False
            elif not check_if_sub_config(f_v, c_v):
                return False
        elif isinstance(f_v, list):
            if not isinstance(c_v, list):
                return False
            if len(c_v) != len(f_v):
                return False
            for c_l_v, f_l_v in zip(c_v, f_v):
                if c_l_v != f_l_v:
                    return False
        elif c_v != f_v:
            return False

    return True


def get_logs(
    start_date: str = None,
    end_date: str = None,
    look_in_folder: str = None,
    filter_config: dict = None,
):
    if look_in_folder is None:
        look_in_folder = "log_bootstrap"
    for path in Path(look_in_folder).rglob("config.yml"):
        # If before start date, ignore.
        if start_date is not None:
            if str(path) < f"{look_in_folder}/{start_date}":
                continue

        # If after end date, ignore.
        if end_date is not None and str(path) > end_date:
            if str(path) > f"{look_in_folder}/{end_date}":
                continue

        # Assert run is successful
        if not (path.parent / "train_accuracies.npy").exists():
            continue

        # Make sure the config matches
        if filter_config is not None:
            with open(path, mode="r") as f:
                config = yaml.load(f)
            if not check_if_sub_config(filter_config, config):
                continue

        yield path


def aggregate_configs(config_paths: List[Union[Path, str]]):
    train_losses = []
    train_accs = []
    val_losses = []
    val_accs = []

    for config_path in config_paths:
        folder_path = Path(config_path).parent
        train_loss = np.load(folder_path / "train_losses.npy")
        train_acc = np.load(folder_path / "train_accuracies.npy")
        val_loss = np.load(folder_path / "val_losses.npy")
        val_acc = np.load(folder_path / "val_accuracies.npy")

        train_accs.append(train_acc)
        train_losses.append(train_loss)
        val_accs.append(val_acc)
        val_losses.append(val_loss)

    train_losses = np.array(train_losses)
    train_accs = np.array(train_accs)
    val_losses = np.array(val_losses)
    val_accs = np.array(val_accs)

    return (train_losses, val_losses), (train_accs, val_accs)


def load_from_config_bootstrap(config_path: Union[Path, str]):
    folder_path = Path(config_path).parent

    train_loss = np.load(folder_path / "train_losses.npy")
    train_acc = np.load(folder_path / "train_accuracies.npy")
    val_loss = np.load(folder_path / "val_losses.npy")
    val_acc = np.load(folder_path / "val_accuracies.npy")

    return (train_loss, val_loss), (train_acc, val_acc)


def get_num_parameters(config_path: Union[Path, str]):
    tmp = init_from_config_path(config_path)
    model = tmp[1]
    return model.num_parameters


def make_akaike_plots(losses, accuracies, num_params, num_qubits, labels):
    for (train_loss, val_loss), label in zip(losses, labels):
        median_train = np.percentile(train_loss, 50, axis=0)
        median_val = np.percentile(val_loss, 50, axis=0)

        plt.plot(median_train, label=label + " Train")
        plt.plot(median_val, label=label + " Validation")

    plt.xlabel("Epochs")
    plt.ylabel("Cross Entropy Loss")
    plt.title("Loss of MNIST")
    plt.legend()
    plt.savefig("plots/mnist_comparison_loss.png", bbox_inches="tight")
    plt.cla()
    plt.clf()

    for (train_acc, val_acc), label in zip(accuracies, labels):
        median_train = np.percentile(train_acc, 50, axis=0)
        median_val = np.percentile(val_acc, 50, axis=0)

        plt.plot(median_train, label=label + " Train")
        plt.plot(median_val, label=label + " Validation")

    plt.xlabel("Epochs")
    plt.ylabel("Accuracy")
    plt.title("Accuracy of MNIST")
    plt.legend()
    plt.savefig("plots/mnist_comparison_acc.png", bbox_inches="tight")
    plt.cla()
    plt.clf()


def main():
    path_to_classical_45_6_config = Path(
        "20_05_08_log_bootstrap/2020-05-10_16:28:19/config.yml"
    )
    path_to_quantum_45_6_config = Path("log_bootstrap/2020-05-12_13:11:12/config.yml")

    path_to_classical_63_4_config = Path(
        "20_05_08_log_bootstrap/2020-05-10_17:33:33/config.yml"
    )
    path_to_quantum_63_4_config = Path("log_bootstrap/2020-05-03_21:54:53/config.yml")

    with open(path_to_quantum_45_6_config, mode="r") as f:
        quantum_45_6 = yaml.load(f)
    with open(path_to_quantum_63_4_config, mode="r") as f:
        quantum_63_4 = yaml.load(f)

    quantum_63_4_configs = list(
        get_logs(
            start_date="2020-05-03",
            end_date="2020-05-17",
            look_in_folder="log",
            filter_config=quantum_63_4,
        )
    )
    quantum_45_6_configs = list(
        get_logs(
            start_date="2020-05-03",
            end_date="2020-05-17",
            look_in_folder="log",
            filter_config=quantum_45_6,
        )
    )

    # Get arrays of losses
    class_63_4_losses, class_63_4_accs = load_from_config_bootstrap(
        path_to_classical_63_4_config
    )
    quant_63_4_losses, quant_63_4_accs = aggregate_configs(quantum_63_4_configs)
    class_45_6_losses, class_45_6_accs = load_from_config_bootstrap(
        path_to_classical_45_6_config
    )
    quant_45_6_losses, quant_45_6_accs = aggregate_configs(quantum_45_6_configs)

    # class_63_4_num_params = get_num_parameters(path_to_classical_63_4_config)
    # quant_63_4_num_params = get_num_parameters(path_to_quantum_63_4_config)
    # class_45_6_num_params = get_num_parameters(path_to_classical_45_6_config)
    # quant_45_6_num_params = get_num_parameters(path_to_quantum_45_6_config)

    # Get number of parameters
    make_akaike_plots(
        [class_63_4_losses, class_45_6_losses, quant_63_4_losses, quant_45_6_losses],
        [class_63_4_accs, class_45_6_accs, quant_63_4_accs, quant_45_6_accs],
        # [class_63_4_num_params, class_45_6_num_params, quant_63_4_num_params, quant_45_6_num_params ],
        [None, None, None, None],
        [0, 0, 4, 6],
        ["Classical 63/4", "Classical 45/6", "Quantum 63/4", "Quantum 45/6"],
    )


if __name__ == "__main__":
    main()
