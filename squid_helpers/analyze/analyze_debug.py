import os
from glob import glob
from pathlib import Path
from typing import List

import numpy as np
import yaml
from matplotlib import pyplot as plt


def __util_flatten_list_of_numpy_arrays(arr: List[np.ndarray]):
    tmp = []
    for a in arr:
        tmp.extend(a.flatten())
    return np.array(tmp)


def standard_configs():
    for path in glob("log/**/config.yml"):
        print(path)
        if path.startswith("log/2020-03-03"):
            folder_path = Path(path).parent

            if not (folder_path / "last_model.pth").exists():
                continue

            train_losses = np.load(folder_path / "train_losses.npy")
            train_accs = np.load(folder_path / "train_accuracies.npy")
            val_losses = np.load(folder_path / "val_losses.npy") / 1250
            val_accs = np.load(folder_path / "val_accuracies.npy")

            epochs = list(range(len(train_losses)))

            plt.plot(epochs, train_losses, label="Train")
            plt.plot(epochs, val_losses, label="Validation")
            plt.title(f"Losses: {path}")
            plt.xlabel("Epochs")
            plt.ylabel("CE Loss")
            plt.legend()
            plt.show()

            plt.plot(epochs, train_accs, label="Train")
            plt.plot(epochs, val_accs, label="Validation")
            plt.title(f"Accuracy: {path}")
            plt.xlabel("Epochs")
            plt.ylabel("Accuracy")
            plt.legend()
            plt.show()


def bootstrap_configs():
    for path in Path("log_bootstrap").rglob("config.yml"):
        if str(path) < "log_bootstrap/2020-04-19_12":
            continue
        if not (path.parent / "batch_losses.npy").exists():
            continue

        print(path)

        folder_path = Path(path).parent

        train_losses = np.load(folder_path / "train_losses.npy")
        train_accs = np.load(folder_path / "train_accuracies.npy")
        val_losses = np.load(folder_path / "val_losses.npy")
        val_accs = np.load(folder_path / "val_accuracies.npy")

        # These are of shape:
        # (runs, batches, model specific dimensions (or 1 for losses))
        batch_losses = np.load(folder_path / "batch_losses.npy")
        batch_weights = np.load(folder_path / "batch_weights.npy", allow_pickle=True)
        batch_gradients = np.load(
            folder_path / "batch_gradients.npy", allow_pickle=True
        )

        total_max_diff_weight = []
        total_max_diff_grad = []
        total_max_grad = []
        # Calculate various statistics
        for run_weight_batches, run_grad_batches in zip(batch_weights, batch_gradients):
            run_max_diff_weight = []
            run_max_diff_grad = []
            run_max_grad = []

            prev_weight = __util_flatten_list_of_numpy_arrays(
                run_weight_batches[0]
            ).copy()
            prev_grad = __util_flatten_list_of_numpy_arrays(run_grad_batches[0]).copy()

            run_max_grad.append(np.max(np.abs(prev_grad)))
            for batch_weight, batch_grad in zip(
                run_weight_batches[1:], run_grad_batches[1:]
            ):
                batch_weight = __util_flatten_list_of_numpy_arrays(batch_weight)
                batch_grad = __util_flatten_list_of_numpy_arrays(batch_grad)

                # Differences to previous batches
                max_diff_grad = np.max(np.abs(batch_grad - prev_grad))
                max_diff_weight = np.max(np.abs(batch_weight - prev_weight))

                # Max size of gradient jump/or ratio of weights
                max_grad = np.max(np.abs(batch_grad))

                run_max_diff_weight.append(max_diff_weight)
                run_max_diff_grad.append(max_diff_grad)
                run_max_grad.append(max_grad)

                prev_weight = batch_weight.copy()
                prev_grad = batch_grad.copy()

            total_max_diff_weight.append(run_max_diff_weight)
            total_max_diff_grad.append(run_max_diff_grad)
            total_max_grad.append(run_max_grad)

        total_max_diff_weight = np.array(total_max_diff_weight)
        total_max_diff_grad = np.array(total_max_diff_grad)
        total_max_grad = np.array(total_max_grad)

        # Get some plot specific values
        with open(path, mode="r") as f:
            config = yaml.safe_load(f)

        k = config["model 1"]["args"]["layers"][0]

        labels = "_".join(map(str, config["dataset"]["args"]["labels"]))

        epochs = list(range(train_losses.shape[1]))
        x_labels_batches = np.linspace(0, len(train_losses), batch_losses.shape[1])

        # Medians, Percentiles etc.
        low_train_loss = np.percentile(train_losses, 25, axis=0)
        med_train_loss = np.percentile(train_losses, 50, axis=0)
        high_train_loss = np.percentile(train_losses, 75, axis=0)
        low_train_acc = np.percentile(train_accs, 25, axis=0)
        med_train_acc = np.percentile(train_accs, 50, axis=0)
        high_train_acc = np.percentile(train_accs, 75, axis=0)

        low_val_loss = np.percentile(val_losses, 25, axis=0)
        med_val_loss = np.percentile(val_losses, 50, axis=0)
        high_val_loss = np.percentile(val_losses, 75, axis=0)
        low_val_acc = np.percentile(val_accs, 25, axis=0)
        med_val_acc = np.percentile(val_accs, 50, axis=0)
        high_val_acc = np.percentile(val_accs, 75, axis=0)

        low_max_diff_weight = np.percentile(total_max_diff_weight, 25, axis=0)
        med_max_diff_weight = np.percentile(total_max_diff_weight, 50, axis=0)
        high_max_diff_weight = np.percentile(total_max_diff_weight, 75, axis=0)

        low_max_diff_grad = np.percentile(total_max_diff_grad, 25, axis=0)
        med_max_diff_grad = np.percentile(total_max_diff_grad, 50, axis=0)
        high_max_diff_grad = np.percentile(total_max_diff_grad, 75, axis=0)

        low_max_grad = np.percentile(total_max_grad, 25, axis=0)
        med_max_grad = np.percentile(total_max_grad, 50, axis=0)
        high_max_grad = np.percentile(total_max_grad, 75, axis=0)

        low_batch_losses = np.percentile(batch_losses, 25, axis=0)
        med_batch_losses = np.percentile(batch_losses, 50, axis=0)
        high_batch_losses = np.percentile(batch_losses, 75, axis=0)

        # Do plotting
        plt.plot(x_labels_batches, med_batch_losses, label="Train", color="g")
        plt.fill_between(
            x_labels_batches, low_batch_losses, high_batch_losses, color="g", alpha=0.5
        )
        plt.title(f"Loss per Batch IQR k: {k}")
        plt.xlabel("Epochs")
        plt.ylabel("CE Loss")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_batch_loss.png", bbox_inches="tight")
        plt.cla()
        plt.clf()

        plt.plot(x_labels_batches, med_max_grad, label="Train", color="g")
        plt.fill_between(
            x_labels_batches, low_max_grad, high_max_grad, color="g", alpha=0.5
        )
        plt.title("Maximum (unsigned) value of gradient per batch")
        plt.xlabel("Epochs")
        plt.ylabel("Gradient Value")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_batch_grad_max.png", bbox_inches="tight")
        plt.cla()
        plt.clf()

        plt.plot(x_labels_batches[1:], med_max_diff_grad, label="Train", color="g")
        plt.fill_between(
            x_labels_batches[1:],
            low_max_diff_grad,
            high_max_diff_grad,
            color="g",
            alpha=0.5,
        )
        plt.title("Max different between gradient batches")
        plt.xlabel("Epochs")
        plt.ylabel("Gradient Difference")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_batch_grad_diff.png", bbox_inches="tight")
        plt.cla()
        plt.clf()

        plt.plot(x_labels_batches[1:], med_max_diff_weight, label="Train", color="g")
        plt.fill_between(
            x_labels_batches[1:],
            low_max_diff_weight,
            high_max_diff_weight,
            color="g",
            alpha=0.5,
        )
        plt.title("Max different between weight batches")
        plt.xlabel("Epochs")
        plt.ylabel("Weight Difference")
        plt.legend()
        plt.savefig(
            f"plots/l_{labels}_k_{k}_batch_weight_diff.png", bbox_inches="tight"
        )
        plt.cla()
        plt.clf()

        plt.plot(epochs, med_train_loss, label="Train", color="g")
        plt.plot(epochs, med_val_loss, label="Validation", color="orange")
        plt.fill_between(epochs, low_train_loss, high_train_loss, color="g", alpha=0.5)
        plt.fill_between(epochs, low_val_loss, high_val_loss, color="orange", alpha=0.5)
        plt.title(f"Loss IQR k: {k}")
        plt.xlabel("Epochs")
        plt.ylabel("CE Loss")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_loss.png", bbox_inches="tight")
        plt.cla()
        plt.clf()

        plt.plot(epochs, med_train_acc, label="Train", color="g")
        plt.plot(epochs, med_val_acc, label="Validation", color="orange")
        plt.fill_between(epochs, low_train_acc, high_train_acc, color="g", alpha=0.5)
        plt.fill_between(epochs, low_val_acc, high_val_acc, color="orange", alpha=0.5)
        plt.title(f"Accuracy IQR k: {k}")
        plt.xlabel("Epochs")
        plt.ylabel("Accuracy")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_acc.png", bbox_inches="tight")
        plt.cla()
        plt.clf()


if __name__ == "__main__":
    if not os.path.exists("plots"):
        os.makedirs("plots")

    # standard_configs()
    bootstrap_configs()
