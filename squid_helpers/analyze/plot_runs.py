import os
from glob import glob
from pathlib import Path

import numpy as np
import yaml
from matplotlib import pyplot as plt


def standard_configs():
    for path in glob("log/**/config.yml"):
        print(path)
        if path.startswith("log/2020-03-03"):
            folder_path = Path(path).parent

            if not (folder_path / "last_model.pth").exists():
                continue

            train_losses = np.load(folder_path / "train_losses.npy")
            train_accs = np.load(folder_path / "train_accuracies.npy")
            val_losses = np.load(folder_path / "val_losses.npy") / 1250
            val_accs = np.load(folder_path / "val_accuracies.npy")

            epochs = list(range(len(train_losses)))

            plt.plot(epochs, train_losses, label="Train")
            plt.plot(epochs, val_losses, label="Validation")
            plt.title(f"Losses: {path}")
            plt.xlabel("Epochs")
            plt.ylabel("CE Loss")
            plt.legend()
            plt.show()

            plt.plot(epochs, train_accs, label="Train")
            plt.plot(epochs, val_accs, label="Validation")
            plt.title(f"Accuracy: {path}")
            plt.xlabel("Epochs")
            plt.ylabel("Accuracy")
            plt.legend()
            plt.show()


def bootstrap_configs():
    for path in Path("log_bootstrap").rglob("config.yml"):
        if not "log_bootstrap/2020-04-10_" < str(path) < "log_bootstrap/2020-04-14_12":
            continue

        with open(path, mode="r") as f:
            config = yaml.safe_load(f)

        k = config["model 1"]["args"]["layers"][0]

        if config["dataset"]["args"]["labels"] is None:
            continue
        labels = "_".join(map(str, config["dataset"]["args"]["labels"]))

        print(path)

        folder_path = Path(path).parent

        train_losses = np.load(folder_path / "train_losses.npy")
        train_accs = np.load(folder_path / "train_accuracies.npy")
        val_losses = np.load(folder_path / "val_losses.npy")
        val_accs = np.load(folder_path / "val_accuracies.npy")

        epochs = list(range(train_losses.shape[1]))

        low_train_loss = np.percentile(train_losses, 25, axis=0)
        med_train_loss = np.percentile(train_losses, 50, axis=0)
        high_train_loss = np.percentile(train_losses, 75, axis=0)
        low_train_acc = np.percentile(train_accs, 25, axis=0)
        med_train_acc = np.percentile(train_accs, 50, axis=0)
        high_train_acc = np.percentile(train_accs, 75, axis=0)

        low_val_loss = np.percentile(val_losses, 25, axis=0)
        med_val_loss = np.percentile(val_losses, 50, axis=0)
        high_val_loss = np.percentile(val_losses, 75, axis=0)
        low_val_acc = np.percentile(val_accs, 25, axis=0)
        med_val_acc = np.percentile(val_accs, 50, axis=0)
        high_val_acc = np.percentile(val_accs, 75, axis=0)

        plt.plot(epochs, med_train_loss, label="Train", color="g")
        plt.plot(epochs, med_val_loss, label="Validation", color="orange")
        plt.fill_between(epochs, low_train_loss, high_train_loss, color="g", alpha=0.5)
        plt.fill_between(epochs, low_val_loss, high_val_loss, color="orange", alpha=0.5)
        plt.title(f"Loss IQR k: {k}")
        plt.xlabel("Epochs")
        plt.ylabel("CE Loss")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_loss.png", bbox_inches="tight")
        plt.cla()
        plt.clf()

        plt.plot(epochs, med_train_acc, label="Train", color="g")
        plt.plot(epochs, med_val_acc, label="Validation", color="orange")
        plt.fill_between(epochs, low_train_acc, high_train_acc, color="g", alpha=0.5)
        plt.fill_between(epochs, low_val_acc, high_val_acc, color="orange", alpha=0.5)
        plt.title(f"Accuracy IQR k: {k}")
        plt.xlabel("Epochs")
        plt.ylabel("Accuracy")
        plt.legend()
        plt.savefig(f"plots/l_{labels}_k_{k}_acc.png", bbox_inches="tight")
        plt.cla()
        plt.clf()


if __name__ == "__main__":
    if not os.path.exists("plots"):
        os.makedirs("plots")

    # standard_configs()
    bootstrap_configs()
