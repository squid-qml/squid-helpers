from typing import Any, Callable, Dict, Tuple

from torch import Tensor

from . import identity, to_angle

Transform = Callable[[Tensor], Tensor]


def _get_single_transform(func_str: str) -> Tuple[Transform, str]:
    func_str_lower = func_str.lower().replace("_", "-")

    func: Transform

    if func_str_lower in [
        "angle",
        "to angle",
        "to_angle",
        "to-angle",
        "range",
        "to range",
        "to_range",
        "to-range",
    ]:
        func = to_angle.transform
        func_name = "angle"
    elif func_str_lower in [
        "none",
        "identity",
    ]:
        func = identity.transform
        func_name = "identity"

    return func, func_name


def get_transforms(
    config: Dict[str, Any]
) -> Tuple[Tuple[Transform, str], Tuple[Transform, str]]:
    model_1_2_func = config.get("model 1 2 func", "identity")
    model_2_3_func = config.get("model 2 3 func", "identity")

    return (
        _get_single_transform(model_1_2_func),
        _get_single_transform(model_2_3_func),
    )
