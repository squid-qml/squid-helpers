from math import pi

from torch import Tensor, nn

sigmoid = nn.Sigmoid()


def transform(x: Tensor) -> Tensor:
    x = sigmoid(x)
    x *= 2.0 * pi
    return x
