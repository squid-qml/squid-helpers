import os
import pickle

import numpy as np
from sklearn.model_selection import train_test_split

from .__internal_utils import norm_angle, warn_about_non_null_kwargs


def load_data(
    qcd_path: str = None,
    higgs_path: str = None,
    feature_keys: list = None,
    train_size: int = None,
    val_size: int = None,
    test_size: int = None,
    random_seed: int = None,
    normalize: bool = True,
    debug: bool = False,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):
    # Warnings
    warn_about_non_null_kwargs(kwargs)

    # Set defaults
    if qcd_path is None:
        qcd_path = "data/higgs_qcd/qcd_100000_pt_250_500.pkl"
    if higgs_path is None:
        higgs_path = "data/higgs_qcd/higgs_100000_pt_250_500.pkl"
    if feature_keys is None:
        feature_keys = ["mass", "d2"]

    if not os.path.exists(qcd_path):
        raise ValueError(f"Provided qcd path ({qcd_path}) does not exist.")
    if not os.path.exists(higgs_path):
        raise ValueError(f"Provided higgs path ({higgs_path}) does not exist.")

    # Load raw data
    with open(qcd_path, "rb") as f:
        data_qcd = pickle.load(f)
    with open(higgs_path, "rb") as f:
        data_higgs = pickle.load(f)

    # check that keys are present
    for key in feature_keys:
        if key not in data_qcd:
            raise ValueError(f"Can't find key {key} in qcd data file {qcd_path}")
        if key not in data_higgs:
            raise ValueError(f"Can't find key {key} in higgs data file {higgs_path}")

    total_size = len(data_higgs) + len(data_higgs)
    if train_size is None:
        if val_size is not None or test_size is not None:
            print("Either all sizes have to be None or not None.\n")
            raise ValueError("Invalid sizes in load_data in qcd_higgs")

        train_size = int(0.6 * total_size)
        test_size = int(0.2 * total_size)
        val_size = total_size - train_size - test_size
    else:
        if val_size is None or test_size is None:
            print("Either all sizes have to be None or not None.\n")
            raise ValueError("Invalid sizes in load_data in qcd_higgs")

        if val_size + train_size + test_size > total_size:
            val_size = total_size - train_size - test_size
            if val_size < 0:
                raise ValueError(
                    "train_size and test_size are bigger than size of given dataset."
                )
            else:
                print(
                    "train_size, val_size and test_size were bigger than size of given dataset.\n"
                    "Decreased val_size to maximal possible value."
                )

    return __get_datasets(
        train_size,
        val_size,
        test_size,
        data_qcd,
        data_higgs,
        feature_keys,
        seed=random_seed,
        normalize=normalize,
        debug=debug,
    )


# this function trims the input to appropriate size and generates the datasets for classification
# - when normalize is True the data is mapped into [0,2pi]^n
def __get_datasets(
    train_size,
    val_size,
    test_size,
    data_qcd,
    data_higgs,
    feature_keys,
    seed=897,
    normalize=True,
    debug=False,
):
    tot_size = sum((train_size, val_size, test_size))
    len_qcd = data_qcd.shape[0]
    len_higgs = data_higgs.shape[0]

    # - Higgs signal
    size_signal = tot_size // 2

    if size_signal > len_higgs:
        raise ValueError(
            f"Requested {size_signal} higgs data but only got {len_higgs} from input"
        )

    X_signal = np.zeros((size_signal, len(feature_keys)))
    y_signal = np.ones(X_signal.shape[0], dtype=int)

    for idx, feat in enumerate(feature_keys):
        X_signal[:, idx] = data_higgs[feat][:size_signal]
    if debug:
        print("Foreground")
        print(X_signal, y_signal)

    # - QCD background
    size_background = tot_size - tot_size // 2
    if size_background > len_qcd:
        raise ValueError(
            f"Requested {size_background} qcd data but only got {len_qcd} from input"
        )
    X_background = np.zeros((size_background, len(feature_keys)))
    y_background = np.zeros(X_background.shape[0], dtype=int)
    for idx, key in enumerate(feature_keys):
        X_background[:, idx] = data_qcd[key][:size_background]
    if debug:
        print("Background")
        print(X_background, y_background)

    # normalize data
    if normalize:
        for idx in range(len(feature_keys)):
            X_signal[:, idx], X_background[:, idx] = norm_angle(
                X_signal[:, idx], X_background[:, idx], full=True
            )

    # concatenate data
    X = np.concatenate([X_background, X_signal], axis=0)
    y = np.concatenate([y_background, y_signal])

    # shuffle data and prepare datasets
    X, X_test, y, y_test = train_test_split(
        X,
        y,
        stratify=y,
        random_state=seed,
        train_size=train_size + val_size,
        test_size=test_size,
        shuffle=True,
    )
    X_train, X_val, y_train, y_val = train_test_split(
        X,
        y,
        stratify=y,
        random_state=seed,
        train_size=train_size,
        test_size=val_size,
        shuffle=True,
    )

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
