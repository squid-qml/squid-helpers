import os
import shutil

import numpy as np
import wget

from .__internal_utils import convert_to_ints, warn_about_non_null_kwargs


def load_data(
    train_size: int,
    val_size: int,
    random_seed: int = None,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):
    if train_size is None or val_size is None:
        raise ValueError("train_size and val_size cannot be null in census dataset.")

    # Warnings
    warn_about_non_null_kwargs(kwargs)

    # Download file if needed
    if not os.path.exists("data/census.data") or not os.path.exists(
        "data/census_test.data"
    ):
        # Train dataset
        # Download file
        print("Downloading Census Train Dataset")
        wget.download(
            "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.data"
        )
        print(
            ""
        )  # wget does not make a new line. Purely aesthetic reason for this print

        # Move and extract it
        shutil.move("adult.data", "data/census.data")

        # Test dataset
        # Download file
        print("Downloading Census Test Dataset")
        wget.download(
            "https://archive.ics.uci.edu/ml/machine-learning-databases/adult/adult.test"
        )
        print(
            ""
        )  # wget does not make a new line. Purely aesthetic reason for this print

        # Move and extract it
        shutil.move("adult.test", "data/census_test.data")

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Get the x and y
    X = np.loadtxt("data/census.data", dtype=str, delimiter=",")
    X_test = np.loadtxt("data/census_test.data", dtype=str, delimiter=",", skiprows=1)

    X = convert_to_ints(X[:, :-1])
    X_test = convert_to_ints(X_test[:, :-1])

    y = np.loadtxt("data/census.data", dtype=str, delimiter=",")
    y = y[:, -1]
    y_test = np.loadtxt("data/census_test.data", dtype=str, delimiter=",", skiprows=1)
    y_test = y_test[:, -1]

    # Convert to int indices
    y = np.where(y == " <=50K", 0, 1)
    y_test = np.where(y_test == " <=50K", 0, 1)

    # Shuffle. So claim in next step is True
    perm = np.random.permutation(len(X))
    X = X[perm]
    y = y[perm]

    # Since the arrays themselves are random we can just take slices.
    X_train = X[:train_size]
    y_train = y[:train_size]

    X_val = X[-val_size:]
    y_val = y[-val_size:]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
