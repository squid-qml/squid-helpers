import warnings
from typing import Any, Dict, Tuple

import numpy as np

DatasetSliceType = Tuple[np.ndarray, np.ndarray]
DatasetType = Tuple[DatasetSliceType, DatasetSliceType, DatasetSliceType]


def norm_angle(a, b, tight=True, full=False):
    """ Normalize pair of data sets (eg. m and d2 for higgs-qcd)
        By default the data is mapped to [0,pi]^2, this can be
        changed into [0,2pi]^2 enabling the option 'full'
    """
    ubound = max(max(a), max(b))
    lbound = min(min(a), min(b))
    new_a = a
    new_b = b
    if tight:
        new_a -= lbound
        new_b -= lbound
        ubound -= lbound
    new_a *= np.pi / ubound
    new_b *= np.pi / ubound
    if full:
        new_a = 2.0 * new_a
        new_b = 2.0 * new_b
    return (new_a, new_b)


def convert_to_ints(arr):
    for idx in range(arr.shape[1]):
        try:
            tmp = np.array(arr[:, idx], dtype=float)
        except ValueError:
            try:
                tmp = np.array(arr[:, idx], dtype=int)
            except ValueError:
                unique_things = np.unique(arr[:, idx])
                unique_dict = dict(zip(unique_things, list(range(len(unique_things)))))
                tmp = np.vectorize(lambda x: unique_dict[x])(arr[:, idx])
        arr[:, idx] = tmp

    return np.array(arr, dtype=float)


def warn_about_non_null_kwargs(d: Dict[str, Any]):
    non_null_keys = []

    for k, v in d.items():
        if k != "debug" and v is not None:
            non_null_keys.append(k)

    if len(non_null_keys) > 0:
        warnings.warn(
            f'Keys: {", ".join(non_null_keys)}\n'
            "are not going to be used in the dataset function you specified.\n"
            "Please make sure that there is no behavior you expected to see, which did not happen.",
            category=UserWarning,
            stacklevel=5,
        )
