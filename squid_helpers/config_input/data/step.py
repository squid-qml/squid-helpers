import numpy as np
from scipy.special import expit

from .__internal_utils import norm_angle, warn_about_non_null_kwargs


def load_data(
    num_features: int,
    train_size: int,
    val_size: int,
    test_size: int,
    random_seed: int = None,
    normalize: bool = True,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):
    if (
        train_size is None
        or val_size is None
        or test_size is None
        or num_features is None
    ):
        raise ValueError(
            "train_size, val_size, test_size and num_features cannot be null in step dataset."
        )

    # Warnings
    warn_about_non_null_kwargs(kwargs)

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Get the x and y
    X = np.random.randn(train_size + val_size + test_size, num_features)
    y = np.where(np.sum(X, axis=1) > 0, 1, 0)

    if normalize:
        # Split into signal and background
        X_signal = X[y == 1]
        y_signal = y[y == 1]

        X_background = X[y == 0]
        y_background = y[y == 0]

        # Normalize both
        for idx in range(num_features):
            X_signal[:, idx], X_background[:, idx] = norm_angle(
                X_signal[:, idx], X_background[:, idx], full=True
            )

        # Combine back
        X = np.vstack((X_signal, X_background))
        y = np.vstack((y_signal, y_background))

        # Reshuffle. So claim in next step is True
        perm = np.random.permutation(len(X))
        X = X[perm]
        y = y[perm]

    # Since the arrays themselves are random we can just take slices.
    X_train = X[:train_size]
    y_train = y[:train_size]

    X_val = X[train_size:-test_size]
    y_val = y[train_size:-test_size]

    X_test = X[-test_size:]
    y_test = y[-test_size:]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)


def load_noisy(
    num_features: int,
    train_size: int,
    val_size: int,
    test_size: int,
    random_seed: int = None,
    normalize: bool = True,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Get the x and y
    X = np.random.randn(train_size + val_size + test_size, num_features)
    noise = np.random.rand(X.shape[0])  # Make an (sum(x), y) points
    y = np.where(
        expit(np.sum(X, axis=1)) > noise, 1, 0
    )  # 1 if over sigmoid curve over point, 0 if under

    if normalize:
        # Split into signal and background
        X_signal = X[y == 1]
        y_signal = y[y == 1]

        X_background = X[y == 0]
        y_background = y[y == 0]

        # Normalize both
        for idx in range(num_features):
            X_signal[:, idx], X_background[:, idx] = norm_angle(
                X_signal[:, idx], X_background[:, idx], full=True
            )

        # Combine back
        X = np.vstack((X_signal, X_background))
        y = np.vstack((y_signal, y_background))

        # Reshuffle. So claim in next step is True
        perm = np.random.permutation(len(X))
        X = X[perm]
        y = y[perm]

    # Since the arrays themselves are random we can just take slices.
    X_train = X[:train_size]
    y_train = y[:train_size]

    X_val = X[train_size:-test_size]
    y_val = y[train_size:-test_size]

    X_test = X[-test_size:]
    y_test = y[-test_size:]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
