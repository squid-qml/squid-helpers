import os
import shutil

import numpy as np
import wget

from .__internal_utils import warn_about_non_null_kwargs


def load_data(
    train_size: int,
    val_size: int,
    test_size: int,
    random_seed: int = None,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):
    if train_size is None or val_size is None or test_size is None:
        raise ValueError(
            "train_size, val_size and test_size cannot be null in wine dataset."
        )

    # Warnings
    warn_about_non_null_kwargs(kwargs)

    # Download file if needed
    if not os.path.exists("data/wine.data"):
        # Download file
        print("Downloading Wine Quality Dataset")
        wget.download(
            "https://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data"
        )
        print(
            ""
        )  # wget does not make a new line. Purely aesthetic reason for this print

        # Move it
        shutil.move("wine.data", "data/wine.data")

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Get the data
    X = np.loadtxt("data/wine.data", delimiter=",")

    # Convert to int indices, and remove labels from features
    y = np.array(X[:, 0], dtype=int)
    X = np.array(X[:, 1:])

    # Shuffle. So claim in next step is True
    perm = np.random.permutation(len(X))
    X = X[perm]
    y = y[perm]

    # Since the arrays themselves are random we can just take slices.
    X_train = X[:train_size]
    y_train = y[:train_size]

    X_val = X[train_size:-test_size]
    y_val = y[train_size:-test_size]

    X_test = X[-test_size:]
    y_test = y[-test_size:]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
