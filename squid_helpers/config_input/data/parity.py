import numpy as np

from .__internal_utils import norm_angle, warn_about_non_null_kwargs


def load_data(
    num_features: int,
    train_size: int,
    val_size: int,
    test_size: int,
    random_seed: int = None,
    normalize: bool = True,
    parity: str = None,
    *args,
    **kwargs,  # Basically make stuff compatible with many different key-words
):
    if train_size is None or val_size is None or test_size is None:
        raise ValueError(
            "train_size, val_size and test_size cannot be null in parity dataset."
        )
    if num_features is None:
        raise ValueError("num_features cannot be null in parity dataset.")

    # Warnings
    warn_about_non_null_kwargs(kwargs)

    if parity is None:
        parity = "even"

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Get the x and y
    X = np.random.randint(0, 2, size=(train_size + val_size + test_size, num_features),)
    y = np.where(np.sum(X, axis=1) % 2 == 0, 1, 0)

    X = X.astype("float64")

    # Convert from even parity to odd
    if parity.lower() == "odd":
        y = 1 - y

    if normalize:
        # Split into signal and background
        X_signal = X[y == 1]
        y_signal = y[y == 1]

        X_background = X[y == 0]
        y_background = y[y == 0]

        # Normalize both
        for idx in range(num_features):
            X_signal[:, idx], X_background[:, idx] = norm_angle(
                X_signal[:, idx], X_background[:, idx], full=True
            )

        # Combine back
        X = np.vstack((X_signal, X_background))
        y = np.vstack((y_signal, y_background))

        # Reshuffle. So claim in next step is True
        perm = np.random.permutation(len(X))
        X = X[perm]
        y = y[perm]

    # Since the arrays themselves are random we can just take slices.
    X_train = X[:train_size]
    y_train = y[:train_size]

    X_val = X[train_size:-test_size]
    y_val = y[train_size:-test_size]

    X_test = X[-test_size:]
    y_test = y[-test_size:]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (X_train, y_train), (X_val, y_val), (X_test, y_test)
