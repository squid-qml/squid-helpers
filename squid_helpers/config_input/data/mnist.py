from typing import Union

import numpy as np
from torch.utils.data import DataLoader
from torchvision import datasets, transforms

from .__internal_utils import warn_about_non_null_kwargs


def load_data(
    train_size: int,
    val_size: int,
    random_seed: int = None,
    labels: Union[str, list] = None,
    *args,
    **kwargs,
):
    if train_size is None or val_size is None:
        raise ValueError("train_size, val_size cannot be null in mnist dataset.")

    # Warnings
    warn_about_non_null_kwargs(kwargs)

    # Load train and test datasets.
    train = datasets.MNIST(
        root="./data", train=True, download=True, transform=transforms.ToTensor()
    )
    test = datasets.MNIST(
        root="./data", train=False, download=True, transform=transforms.ToTensor()
    )

    # Split datasets into inputs and labels
    for inputs, targets in DataLoader(train, batch_size=len(train)):
        train_x = inputs.detach().numpy()
        train_y = targets.detach().numpy()
    for inputs, targets in DataLoader(test, batch_size=len(train)):
        test_x = inputs.detach().numpy()
        test_y = targets.detach().numpy()

    # Filter on labels
    if labels is not None:
        # Process labels
        if isinstance(labels, str):
            tmp = []
            for label in labels.split(","):
                tmp.append(int(label))
            labels = tmp
        elif isinstance(labels, list):
            tmp = []
            for label in labels:
                tmp.append(int(label))
            labels = tmp
        else:
            raise ValueError("Invalid labels argument")
        # Get boolean filters
        train_idxs = np.isin(train_y, labels)
        test_idxs = np.isin(test_y, labels)

        # Actually filter x and y arrays
        train_x = train_x[train_idxs]
        train_y = train_y[train_idxs]

        test_x = test_x[test_idxs]
        test_y = test_y[test_idxs]

        # Convert old labels to new (so they go from 0 to k-1, as PyTorch requires)
        old_to_new_label = dict(zip(np.unique(train_y), range(len(np.unique(train_y)))))
        train_y = np.array([old_to_new_label[x] for x in train_y], dtype=int)
        test_y = np.array([old_to_new_label[x] for x in test_y], dtype=int)

    # Figure out if size of train and val is reasonable:
    if train_size + val_size > len(train_x):
        if train_size > len(train_x):
            raise ValueError(f"Train size to big. Maximal value is {len(train_x)}")
        else:
            val_size = len(train_x) - train_size
            print(f"val_size decreased to {val_size}")

    # Get outside state
    outside_state = np.random.get_state()

    # Set the seed
    if random_seed is not None:
        np.random.seed(random_seed)

    # Shuffle data
    perm = np.random.permutation(len(train_x))
    train_x = train_x[perm]
    train_y = train_y[perm]

    # Extract validation and train sets
    val_x = train_x[-val_size:]
    val_y = train_y[-val_size:]

    train_x = train_x[:train_size]
    train_y = train_y[:train_size]

    # Set the outside state back on
    np.random.set_state(outside_state)

    return (train_x, train_y), (val_x, val_y), (test_x, test_y)
