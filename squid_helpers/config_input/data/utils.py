from typing import Tuple

import numpy as np
import torch
from PIL import Image
from torch.utils.data import Dataset

from . import census, iris, mnist, moons, parity, qcd_higgs, step, wine, wine_quality
from .__internal_utils import DatasetType

DATASET_TO_FUNCTION = {
    "census": census.load_data,
    "iris": iris.load_data,
    "mnist": mnist.load_data,
    "moons": moons.load_data,
    "parity": parity.load_data,
    "qcd_higgs": qcd_higgs.load_data,
    "step": step.load_data,
    "step_noisy": step.load_noisy,
    "wine": wine.load_data,
    "wine_quality": wine_quality.load_data,
}


def load_data(dataset: str, *args, **kwargs) -> Tuple[DatasetType, str]:
    if dataset.lower() in {"higgs_qcd", "higgs qcd", "qcd_higgs", "qcd higgs"}:
        dataset = "qcd_higgs"

    elif dataset.lower() in {"step"}:
        dataset = "step"

    elif dataset.lower() in {"step_noisy", "step noisy", "noisy_step", "noisy step"}:
        dataset = "step_noisy"

    elif dataset.lower() in {"half moons", "moons", "half_moons"}:
        dataset = "moons"

    elif dataset.lower() in {"parity"}:
        dataset = "parity"

    elif dataset.lower() in {"iris"}:
        dataset = "iris"

    elif dataset.lower() in {"census", "adult", "50k"}:
        dataset = "census"

    elif dataset.lower() in {"wine quality", "wine-quality", "wine_quality"}:
        dataset = "wine_quality"

    elif dataset.lower() in {"wine"}:
        dataset = "wine"

    elif dataset.lower() in {"mnist"}:
        dataset = "mnist"

    load_function = DATASET_TO_FUNCTION[dataset]
    return load_function(*args, **kwargs), dataset  # type: ignore


class CustomDataset(Dataset):
    def __init__(self, data_x, data_y, transform=None):
        super().__init__()
        self.data_x = data_x
        self.data_y = data_y
        self.transform = transform

    def __len__(self):
        return len(self.data_x)

    def __getitem__(self, idx):
        # Get x
        sample_x = self.data_x[idx]
        if isinstance(sample_x, np.ndarray):
            # Some tranforms needed.
            sample_x = torch.from_numpy(sample_x)

        elif isinstance(sample_x, str):
            # Image. Needed to be loaded.
            sample_x = Image.open(str(sample_x))

        if self.transform is not None:
            sample_x = self.transform(sample_x)

        return sample_x, self.data_y[idx]
