import argparse
import os
import random
import warnings
from pathlib import Path
from typing import Any, Dict, List, Optional, Tuple, Type, Union

import numpy as np
import torch
import torch.cuda
import yaml
from squid.models import (
    VQC,
    ClassicalModel,
    ConvFeedForward,
    FeedForward,
    MainModel,
    SelectSingleOutput,
    SingleModel,
)
from torch import nn
from torch.utils.data import DataLoader

from ..loggers import Logger
from .data import CustomDataset, load_data
from .get_loss_opt import get_loss_opt


def read_configs() -> List[str]:
    """Enables loading mix of files and folders from,
    and returning only .yml or .yaml files contained in those folders (recursively) or files.

    Returns:
        [List[str]] -- List of paths to yaml files contained within folders and files given in command line.
    """
    parser = argparse.ArgumentParser(
        description="Performs Active Learning Labeling task, given config files.",
        add_help=True,
    )
    parser.add_argument(
        "config_file_paths",
        type=str,
        nargs="+",
        help="Relative paths to valid yaml config files.",
    )
    args = parser.parse_args()

    result = []
    for config_path in args.config_file_paths:
        # Check if it exists in a first place.
        if not os.path.exists(config_path):
            print("Path not found. Skipping {}".format(config_path))
        # If dir, then look for yaml files recursively.
        elif os.path.isdir(config_path):
            config_path = Path(config_path)
            for config_file_path in config_path.rglob("*.yml"):
                result.append(str(config_file_path))
            for config_file_path in config_path.rglob("*.yaml"):
                result.append(str(config_file_path))
        # If file just check if the extension matches.
        elif os.path.isfile(config_path):
            config_path = str(config_path)
            extension = config_path.split(".")[-1]
            if extension in ["yml", "yaml"]:
                result.append(config_path)

    # Make sure there is no overlap.
    result = np.unique(result)

    return result


def init_from_config_path(config_path: Union[str, Path], bootstrap_hash: int = None):
    # Read yaml file into dictionary
    if not os.path.exists(config_path):
        print("Path not found. Skipping {}".format(config_path))
        return
    else:
        with open(config_path, mode="r") as config_file:
            config = yaml.safe_load(config_file)

    return init_from_config(config, bootstrap_hash)


def init_from_config(config: Dict, bootstrap_hash: int = None) -> Tuple[Dict, Dict]:
    # Add debug flag to all 'args' in config
    # We know args will be on the second level
    debug = config["__DEBUG"]
    for k_1, v_1 in config.items():
        if isinstance(v_1, dict):
            if "args" in v_1:
                config[k_1]["args"]["debug"] = debug

    # Add bootstrap hash if specified
    if bootstrap_hash is not None:
        config["BOOTSTRAP HASH"] = bootstrap_hash

    # Read Global Random Seed flag
    # Overwrite all local flags if specified
    if "__RANDOM_SEED" in config and config["__RANDOM_SEED"] is not None:
        __random_seed = config["__RANDOM_SEED"]
        for k, v in config.items():
            if isinstance(v, dict) and "args" in v and "random_seed" not in v["args"]:
                config[k]["args"]["random_seed"] = __random_seed
                __random_seed += 1  # Make initialization not the same

    # Read data, make sure that config changes the name of dataset so it's always the same given type of dataset.
    # i.e. 'higgs_qcd' and 'higgs-qcd' would both become 'qcd_higgs'
    (train_raw_data, val_raw_data, test_raw_data), name = load_data(
        config["dataset"]["type"], **config["dataset"]["args"]
    )
    config["dataset"]["type"] = name
    # Convert data to PyTorch Datasets
    example_inputs, example_targets = train_raw_data
    train = CustomDataset(train_raw_data[0], train_raw_data[1])
    val = CustomDataset(val_raw_data[0], val_raw_data[1])
    test = CustomDataset(test_raw_data[0], test_raw_data[1])

    # Read models

    model = model_from_config(config, example_inputs, example_targets)
    config["model 1"]["type"] = model.model1.__class__.__name__
    config["model 2"]["type"] = model.model2.__class__.__name__
    config["model 3"]["type"] = model.model3.__class__.__name__

    optimizer, optimizer_type, loss, loss_type = get_loss_opt(config, model)

    # Find device
    device = torch.cuda.current_device() if torch.cuda.is_available() else "cpu"

    # Initialize logging function
    logger = Logger(config, model, loss, device)

    # Read batch sizes, epochs etc.
    batch_size = config["batch size"]
    epochs = config["epochs"]
    verbose = config["verbose"]

    # Move to DataLoader
    train_loader = DataLoader(train, batch_size=batch_size)
    validation_loader = DataLoader(val, batch_size=batch_size)
    test_loader = DataLoader(test, batch_size=batch_size)

    config["train"] = config.get("train", True)
    config["test"] = config.get("test", False)

    assert (
        config["train"] or config["test"]
    ), "Either Test or Train have to be chosen to run from a config. (Train is much more common)"

    kwargs = {}

    kwargs["model"] = model
    kwargs["optimizer"] = optimizer
    kwargs["criterion"] = loss
    kwargs["train_data"] = train
    kwargs["val_data"] = val
    kwargs["test_data"] = test
    kwargs["train_loader"] = train_loader
    kwargs["validation_loader"] = validation_loader
    kwargs["test_loader"] = test_loader
    kwargs["epochs"] = epochs
    kwargs["batch_size"] = batch_size
    kwargs["device"] = device
    kwargs["logger"] = logger
    kwargs["verbose"] = verbose

    return (
        config,
        kwargs,
    )


# region: model loading


def _load_single_model(
    model_type: str,
    inputs: np.ndarray,
    targets: Optional[np.ndarray],
    random_seed: Optional[int],
    *args,
    **kwargs,
) -> Tuple[SingleModel, str]:
    # Figure out in_features, and num_classes from example input and output
    in_features = np.prod(inputs[0].shape)

    # Determine between classification and regression.
    # I do not expect us to run it on full ImageNet and stuff.
    # It can be changed to some percentage of len(targets) in future though
    num_classes = None
    if targets is not None:
        num_classes = len(np.unique(targets))

        if num_classes > 200:
            if len(targets.shape) == 1:
                num_classes = 1
            elif len(targets.shape) == 2:
                num_classes = targets.shape[1]
            else:
                num_classes = None

    # Add to kwargs
    if "in_shape" not in kwargs or kwargs["in_shape"] is None:
        kwargs["in_shape"] = list(inputs[0].shape)
    if targets is not None and (
        "out_shape" not in kwargs or kwargs["out_shape"] is None
    ):
        kwargs["out_shape"] = list(targets.shape[1:]) if len(targets.shape) > 1 else [1]
    if "in_features" not in kwargs or kwargs["in_features"] is None:
        kwargs["in_features"] = in_features
    if targets is not None and kwargs.get("num_classes", None) is None:
        kwargs["num_classes"] = num_classes

    # Save the outer random state
    seed_np_state = np.random.get_state()
    seed_torch_state = torch.random.get_rng_state()
    seed_random_state = random.getstate()

    # Initialize seed
    if random_seed is not None:
        np.random.seed(random_seed)
        torch.random.manual_seed(random_seed)
        random.seed(random_seed)

    """
    Put models here
    """
    model_class: Type[SingleModel]
    if model_type.lower().replace("_", "-") in [
        "linear",
        "feed-forward",
        "forward",
    ]:
        model_type = "feed-forward"
        model_class = FeedForward  # type: ignore
    elif model_type.lower().replace("_", "-") in [
        "conv",
        "conv-feed-forward",
        "conv-forward",
    ]:
        model_type = "conv-feed-forward"
        model_class = ConvFeedForward  # type: ignore
    elif model_type.lower().replace("_", "-") in ["vqc", "vqc-multi"]:
        model_type = "vqc"
        model_class = VQC  # type: ignore
    elif model_type.lower().replace("_", "-") in [
        "single",
        "select-single",
        "select-single-output",
        "single-qubit",
    ]:
        model_type = "select-single-output"
        model_class = SelectSingleOutput  # type: ignore
    else:
        raise ValueError("Model type not recognized.")

    # Call the model constructor
    result = model_class(*args, **kwargs)

    # Revert back to outer state
    if random_seed is not None:
        np.random.set_state(seed_np_state)
        torch.random.set_rng_state(seed_torch_state)
        random.setstate(seed_random_state)

    return result, model_type


def model_from_config(
    config: Dict[str, Any], inputs: np.ndarray, targets: np.ndarray
) -> MainModel:
    model_1_config = config["model 1"]
    model_2_config = config["model 2"]
    model_3_config = config["model 3"]

    inputs = torch.from_numpy(inputs).float()

    # Load models seperately
    # Load first and get inputs into second
    model_1: ClassicalModel
    model_3: ClassicalModel
    model_1, model_1_type = _load_single_model(  # type: ignore
        model_1_config["type"], inputs, None, **model_1_config["args"]
    )

    inputs = model_1(inputs)

    model_2, model_2_type = _load_single_model(
        model_2_config["type"], inputs, None, **model_2_config["args"]
    )

    if not isinstance(model_2, nn.Module):
        inputs = model_2.clip(inputs)
        inputs = inputs.detach().cpu().numpy()
    inputs = model_2(inputs)
    if not isinstance(model_2, nn.Module):
        inputs = torch.from_numpy(inputs)

    model_3, model_3_type = _load_single_model(  # type: ignore
        model_3_config["type"], inputs, targets, **model_3_config["args"]
    )

    # Load state dicts for models if applicable
    if "load from" in model_1_config and model_1_config["load from"] is not None:
        overall_model = torch.load(model_1_config["load from"])
        model_1_state_dict = {}
        for k, v in overall_model.items():
            if k.startswith("model1."):
                model_1_state_dict[k[7:]] = v
        try:
            model_1.load_state_dict(model_1_state_dict)
        except Exception as e:
            warnings.warn(f"Failed to load state dict for model 1 with exception:\n{e}")
    if "load from" in model_2_config and model_2_config["load from"] is not None:
        if isinstance(model_2, nn.Module):
            overall_model = torch.load(model_2_config["load from"])
            model_2_state_dict = {}
            for k, v in overall_model.items():
                if k.startswith("model2."):
                    model_2_state_dict[k[7:]] = v
            try:
                model_2.load_state_dict(model_2_state_dict)
            except Exception as e:
                warnings.warn(
                    f"Failed to load state dict for model 2 with exception:\n{e}"
                )
        else:
            warnings.warn(
                "Model 2 is not a torch Module, and hence it cannot be loaded from a different file."
            )
    if "load from" in model_3_config and model_3_config["load from"] is not None:
        overall_model = torch.load(model_3_config["load from"])
        model_3_state_dict = {}
        for k, v in overall_model.items():
            if k.startswith("model3."):
                model_3_state_dict[k[7:]] = v
        try:
            model_3.load_state_dict(model_3_state_dict)
        except Exception as e:
            warnings.warn(f"Failed to load state dict for model 3 with exception:\n{e}")

    # Merge them all into 1 and return
    main_model = MainModel(model_1, model_2, model_3, config=config)
    return main_model


# region end: model loading
