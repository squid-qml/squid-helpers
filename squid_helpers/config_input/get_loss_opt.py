import inspect
from typing import Dict, Tuple, Type

from squid.models import MainModel
from torch import nn, optim

Loss = nn.modules.loss._Loss


def get_loss_opt(
    config: Dict, model: MainModel
) -> Tuple[optim.Optimizer, str, Loss, str]:
    # Get optimizer
    optimizer_type: str = config["optimizer"]["type"]
    optimizer_args = config["optimizer"]["args"]
    optimizer_args.pop("debug", None)

    # Get optimizer. First get class.
    optimizer_class: Type[optim.Optimizer]
    if optimizer_type.lower() == "adam":
        optimizer_class = optim.Adam
        optimizer_type = "adam"
    elif optimizer_type.lower() == "sgd":
        optimizer_class = optim.SGD
        optimizer_type = "sgd"
    else:
        raise ValueError("Unrecognized optimizer type.")

    # Get only valid arguments to it.
    # Read list of valid arguments to the class of the optimizer,
    # and use them as a filter on optimizer_args provided.
    optimizer_filtered_args = {}
    optimizer_class_args = set(inspect.signature(optimizer_class).parameters.keys())
    for k in optimizer_args:
        if k in optimizer_class_args:
            optimizer_filtered_args[k] = optimizer_args[k]

    # Create that optimizer
    optimizer = optimizer_class(
        model.parameters(trainable_only=True), **optimizer_filtered_args
    )

    # Get loss
    loss: Loss
    loss_type = config["loss"]["type"]
    if loss_type.lower() in [
        "mse",
        "mean square error",
        "mean-square-error",
        "mean squared error",
    ]:
        loss = nn.MSELoss()
        loss_type = "mse"
    elif loss_type.lower() in ["ce", "cross entropy", "crossentropy", "cross-entropy"]:
        loss = nn.CrossEntropyLoss()
        loss_type = "ce"
    elif loss_type.lower() in [
        "nll",
        "nllloss" "negative log likelihood",
    ]:
        loss = nn.NLLLoss()
        loss_type = "nll"
    else:
        raise ValueError("Unrecognized loss type.")

    return optimizer, optimizer_type, loss, loss_type
