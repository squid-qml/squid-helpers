import os
import subprocess
import warnings
from datetime import datetime
from pathlib import Path
from typing import Dict, List

import numpy as np
import torch
import yaml
from sklearn.metrics import accuracy_score
from torch import nn
from torch.utils.data import DataLoader
from tqdm import tqdm


class Logger:
    def __init__(self, config: Dict, model: nn.Module, criterion, device) -> None:
        """Logger for running src/main.py
        Allows to track progress of data along both in terms of
        printing and saving state of model per each epoch and
        per each batch.

        Arguments:
            config {Dict} -- Config to work on. (copy of it will be saved)
            model {nn.Module} -- Model which will be trained.
            criterion {} -- One of PyTorch losses.
                Criterion which above model is optimized to.
            device {} -- Device on which Main Model is loaded.
        """
        self.update = 0  # Index of current gradient update
        self.iter = 0  # Index of current epoch
        self.verbose = config["verbose"]
        self.debug = config["__DEBUG"]

        _folder_path = config.get("log folder", None)
        self.folder_path = Path(
            _folder_path
            if _folder_path is not None
            else "log/{date:%Y-%m-%d_%H:%M:%S.%f}".format(date=datetime.now())
        )

        # Add version from git to the config
        __git_version = subprocess.Popen(
            "git log -1", shell=True, stdout=subprocess.PIPE
        ).stdout

        git_version: str = "None"
        if __git_version is not None:
            git_version = __git_version.read().decode("utf-8")
        config["_GIT_VERSION"] = git_version

        if not os.path.exists(self.folder_path):
            os.makedirs(self.folder_path)
        with open(str(self.folder_path / "config.yml"), mode="w") as config_file:
            yaml.dump(config, config_file)

        # Model
        self.model = model
        # Loss function
        self.criterion = criterion
        # Device
        self.device = device

        # Saving of the best model
        self.best_accuracy = 0.0
        self.save_best = bool(config.get("model main", {}).get("save best", False))
        self.save_best_path = None
        if self.save_best and "save best path" in config["model main"]:
            self.save_best_path = config["model main"]["save best path"]

        if self.save_best_path is not None:
            self.save_best_path = Path(self.save_best_path)
            if not self.save_best_path.parent.exists():
                warnings.warn(
                    "Folder in which to save best model does not exist and "
                    "hence such saving will be ignored.\n"
                    "If you set a flag save best model, it still will be saved to:\n"
                    f"{self.folder_path / 'best_model.pth'}"
                )

        # Initialize arrays
        self.train_losses: List[float] = []
        self.train_acc: List[float] = []
        self.val_losses: List[float] = []
        self.val_acc: List[float] = []

    def _print_update(self, running_loss, validation_loader) -> None:
        """Main function for printing into after each batch.
        """
        if self.verbose > 2:
            print(
                f"Running loss. Epoch {self.iter} Update {self.update}: {running_loss}"
            )

    def process_update(
        self, running_loss: float, validation_loader: DataLoader
    ) -> None:
        """Function that updates information about model at the end of each batch.

        Arguments:
            running_loss {float} --
                Loss of model on training dataset during last epoch up to given batch.
            validation_loader {DataLoader} --
                PyTorch DataLoader for validation dataset,
                which can be used for printing/saving information.
        """

        # Call print
        # There is no save, because it would make program extremely slow.
        self._print_update(running_loss, validation_loader)

        if self.debug:
            if not hasattr(self, "batch_losses"):
                self.batch_losses = []
                self.batch_weights = []
                self.batch_gradients = []

            # Read current iteration of parameters
            curr_weights = [
                param.data.numpy().copy() for param in self.model.parameters()
            ]
            curr_gradients = [
                param.grad.numpy().copy()
                for param in self.model.parameters()
                if param.requires_grad
            ]

            self.batch_losses.append(running_loss / (self.update + 1))
            self.batch_weights.append(curr_weights)
            self.batch_gradients.append(curr_gradients)

        # Update vars.
        self.update += 1

    def _print_iter(self):
        """Main function for printing into after each epoch.
        """
        if self.verbose > 0:
            print("")  # Make a new line for aesthetics^{TM}
            print(f"Epoch {self.iter} Loss: {self.train_losses[-1]}")
            print(f"Epoch {self.iter} Accuracy: {self.train_acc[-1]}")
        if self.verbose > 1:
            print(f"Epoch {self.iter} Validation Loss: {self.val_losses[-1]}")
            print(f"Epoch {self.iter} Validation Accuracy: {self.val_acc[-1]}")

    def _update_iter(
        self,
        y_truth: np.ndarray,
        y_pred: np.ndarray,
        running_loss: float,
        validation_loader: DataLoader,
    ) -> None:
        """Main function for calculating and saving information after each epoch.
        """
        self.train_losses.append(running_loss)
        self.train_acc.append(accuracy_score(y_truth, y_pred))

        # Calculate predictions and truths on validation set on current epoch
        self.train_y_pred = np.array(y_pred)
        self.train_y_truth = np.array(y_truth)

        self.val_y_pred = []
        self.val_y_truth = []

        running_loss = 0
        with torch.no_grad():
            for x, y in tqdm(
                validation_loader,
                disable=(self.verbose == 0),
                desc="Validation Evaluation",
                leave=False,
            ):
                x = x.to(self.device)
                y = y.to(self.device)
                y_pred = self.model(x)

                # Decide whether to calculate accuracy (only once)
                y_pred_labels = torch.argmax(y_pred, dim=1)
                self.val_y_truth.extend(y.cpu().tolist())
                self.val_y_pred.extend(y_pred_labels.cpu().tolist())

                running_loss += self.criterion(y_pred, y).item()

        # Normalize with respect to batches
        running_loss /= len(validation_loader) if len(validation_loader) > 0 else 1

        self.val_y_pred = np.array(self.val_y_pred)
        self.val_y_truth = np.array(self.val_y_truth)

        # Populate validation losses and accuracies
        self.val_losses.append(running_loss)
        self.val_acc.append(accuracy_score(self.val_y_truth, self.val_y_pred))

    def process_iter(
        self,
        y_truth: np.ndarray,
        y_pred: np.ndarray,
        running_loss: float,
        validation_loader: DataLoader,
    ) -> None:
        """Function that updates information about model at the end of each epoch.

        Arguments:
            running_loss {float} --
                Loss of model on training dataset during last epoch.
            validation_loader {DataLoader} --
                PyTorch DataLoader for validation dataset,
                which can be used for printing/saving information.
        """
        # Call print and saving
        self._update_iter(y_truth, y_pred, running_loss, validation_loader)
        self._print_iter()

        # Update vars.
        self.update = 0
        self.iter += 1

    def process_end(self) -> None:
        """Prints and/or saves information about the model at the end of the
        run of the whole algorithm.
        """
        np.save(self.folder_path / "train_accuracies.npy", self.train_acc)
        np.save(self.folder_path / "train_losses.npy", self.train_losses)
        np.save(self.folder_path / "val_accuracies.npy", self.val_acc)
        np.save(self.folder_path / "val_losses.npy", self.val_losses)

        if hasattr(self, "batch_losses"):
            np.save(self.folder_path / "batch_train_losses.npy", self.batch_losses)
            np.save(self.folder_path / "batch_weights.npy", self.batch_weights)
            np.save(self.folder_path / "batch_gradients.npy", self.batch_gradients)

        torch.save(self.model.state_dict(), self.folder_path / "last_model.pth")

        if self.save_best:
            torch.save(self.model.state_dict(), self.folder_path / "best_model.pth")

        if self.save_best_path is not None:
            torch.save(self.model.state_dict(), self.save_best_path)

    def process_test(
        self, y_truth: np.ndarray, y_pred: np.ndarray, running_loss: float,
    ):
        np.save(self.folder_path / "test_truth.npy", y_truth)
        np.save(self.folder_path / "test_predictions.npy", y_pred)

        with open(self.folder_path / "test_accuracy_loss.txt") as f:
            f.writelines(
                [
                    f"Accuracy: {accuracy_score(y_truth, y_pred)}\n",
                    f"Loss: {running_loss}\n",
                ]
            )
