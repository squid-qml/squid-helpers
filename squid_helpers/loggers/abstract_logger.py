import abc

import numpy as np
from torch.utils.data import DataLoader


class AbstractLogger(abc.ABC):
    @abc.abstractmethod
    def process_update(
        self, running_loss: float, validation_loader: DataLoader
    ) -> None:
        """Process every single update of the weights of the network.
        This function is called every single batch update.

        :param running_loss: Loss up to (and including) current batch in a current epoch.
        :type running_loss: float
        :param validation_loader: Torch DataLoader of a Validation dataset.
        :type validation_loader: DataLoader
        """

    @abc.abstractmethod
    def process_iter(
        self,
        y_truth: np.ndarray,
        y_pred: np.ndarray,
        running_loss: float,
        validation_loader: DataLoader,
    ) -> None:
        """Process end of the every epoch.

        :param y_truth: Truth output values of the training set from a last epoch.
        :type y_truth: np.ndarray
        :param y_pred: Predicted output values of the training set from a last epoch.
        :type y_pred: np.ndarray
        :param running_loss: Average loss of a current epoch.
        :type running_loss: float
        :param validation_loader: Torch DataLoader of a Validation dataset.
        :type validation_loader: DataLoader
        """

    @abc.abstractmethod
    def process_end(self) -> None:
        """Process end of the experiment run.
        This function is called after all the epochs have been finished.
        """

    @abc.abstractmethod
    def process_test(
        self, y_truth: np.ndarray, y_pred: np.ndarray, running_loss: float,
    ) -> None:
        """Similar to `process_iter`, but is meant to be run specifically for the test dataset
        """
