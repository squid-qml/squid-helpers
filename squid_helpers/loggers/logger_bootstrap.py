import os
import subprocess
from datetime import datetime
from pathlib import Path
from typing import Dict, List

import numpy as np
import yaml

from .logger import Logger


class BootstrapLogger:
    def __init__(self, config: Dict, bootstrap_hash: int) -> None:
        # TODO: Comment it
        """

        Arguments:
            config {Dict} -- Config to work on. (copy of it will be saved)

        """
        # Add hash
        config["BOOTSTRAP HASH"] = bootstrap_hash

        # Add version from git to the config
        __git_log = subprocess.Popen(
            "git rev-parse HEAD", shell=True, stdout=subprocess.PIPE
        ).stdout
        config["_GIT_VERSION"] = "None"
        if __git_log is not None:
            config["_GIT_VERSION"] = __git_log.read().decode("utf-8")

        _folder_path = config.get("log folder", None)
        self.folder_path = Path(
            _folder_path
            if _folder_path is not None
            else "log_bootstrap/{date:%Y-%m-%d_%H:%M:%S}".format(date=datetime.now())
        )

        if not os.path.exists(self.folder_path):
            os.makedirs(self.folder_path)
        with open(str(self.folder_path / "config.yml"), mode="w") as config_file:
            yaml.dump(config, config_file)

        # Initialize arrays
        self.train_losses: List[List[float]] = []
        self.train_acc: List[List[float]] = []
        self.val_losses: List[List[float]] = []
        self.val_acc: List[List[float]] = []

    def process_iter(self, logger: Logger) -> None:
        self.train_acc.append(logger.train_acc)
        self.train_losses.append(logger.train_losses)
        self.val_acc.append(logger.val_acc)
        self.val_losses.append(logger.val_losses)

        if hasattr(logger, "batch_losses"):
            if not hasattr(self, "batch_losses"):
                self.batch_losses = []
                self.batch_weights = []
                self.batch_gradients = []

            self.batch_losses.append(logger.batch_losses)
            self.batch_weights.append(logger.batch_weights)
            self.batch_gradients.append(logger.batch_gradients)

    def process_end(self) -> None:
        """Saves information about the run.
        """
        np.save(self.folder_path / "train_accuracies.npy", self.train_acc)
        np.save(self.folder_path / "train_losses.npy", self.train_losses)
        np.save(self.folder_path / "val_accuracies.npy", self.val_acc)
        np.save(self.folder_path / "val_losses.npy", self.val_losses)

        if hasattr(self, "batch_losses"):
            np.save(self.folder_path / "batch_losses.npy", self.batch_losses)
            np.save(self.folder_path / "batch_weights.npy", self.batch_weights)
            np.save(self.folder_path / "batch_gradients.npy", self.batch_gradients)
