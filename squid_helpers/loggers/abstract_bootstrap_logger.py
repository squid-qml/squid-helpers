import abc

from .abstract_logger import AbstractLogger


class AbstractBootstrapLogger(abc.ABC):
    @abc.abstractmethod
    def process_iter(self, logger: AbstractLogger) -> None:
        """Process iteration of bootstrap process.
        An iteration is defined as a completion of a task,
        which has ran through all the epochs defined in the configuration.

        :param logger: Logger of a single (finished) task.
        :type logger: AbstractLogger
        """

    @abc.abstractmethod
    def process_end(self) -> None:
        """Process the end of bootstrap logger.
        This typically involves saving accumulated arrays of data from an experiment run,
        and printing some aggregations of it.
        """
