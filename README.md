# Scaled QUantum IDentifier (SQUID) Helpers #

**This is the helper repository to the main [SQUID](https://bitbucket.org/squid-qml/squid) repo. Please follow instructions there to install and use the repository.**

This repository allows to automatically run SQUID from configurations in a limited capability, as well as provides some examples of dealing with the outputs of such *config runs*.


## Installation ##
First you will need to follow instructions outlined in README of [SQUID repo](https://bitbucket.org/squid-qml/squid).

### Conda ###
```
conda activate squid
invoke install-required-packages
```

### Pip ###
```
source <environment-name>
invoke install-required-packages --pip
```

### Verify Installation ###
To verify that everything is installed properly please run:
```
python squid_helpers/main.py examples/example_config.yml
```

## Running the Program ##

The main program is located in `squid_helpers/main.py`.

To run it:
```
python squid_helpers/main.py <your yaml files>
```

It can take multiple files, or folders (including nested folders) of yaml files.

### Examples ###

[Folder examples](./examples) contains examples of configuration files. Feel free to reuse when creating custom configuration files.

## Contributing ##

For linting and testing follow the explanations provided at main [SQUID README](https://bitbucket.org/squid-qml/squid).
Please note that helpers do not have as strict testing requirements, however we expect linting rules to be followed.
