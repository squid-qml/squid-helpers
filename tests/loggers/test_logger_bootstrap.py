import subprocess
from datetime import datetime
from pathlib import Path
from unittest.mock import Mock, call, sentinel

import munch
import pytest

from squid_helpers.loggers import BootstrapLogger

MOCK_DATETIME_NOW = datetime(year=2020, month=1, day=1)


@pytest.fixture
def patch_path():
    return "squid_helpers.loggers.logger_bootstrap"


@pytest.fixture
def patch_datetime_now(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.datetime", Mock(now=Mock(return_value=MOCK_DATETIME_NOW)),
    )


@pytest.fixture
def patch_numpy_save(mocker, patch_path):
    return mocker.patch(f"{patch_path}.np.save", Mock(return_value=None),)


@pytest.fixture
def patch_open(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.open",
        Mock(
            return_value=Mock(
                __enter__=Mock(
                    return_value=Mock(
                        writelines=Mock(return_value=sentinel.file_writelines)
                    )
                ),
                __exit__=Mock(return_value=None),
            ),
        ),
    )


@pytest.fixture
def patch_os(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.os",
        Mock(
            path=Mock(exists=Mock(side_effect=lambda x: not str(x) == ".")),
            makedirs=Mock(return_value=sentinel.os_makedirs),
        ),
    )


@pytest.fixture
def patch_subprocess_popen(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.subprocess.Popen",
        Mock(
            return_value=Mock(
                stdout=Mock(
                    read=Mock(
                        return_value=Mock(
                            decode=Mock(return_value=sentinel.git_version)
                        )
                    )
                ),
            ),
        ),
    )


@pytest.fixture
def patch_yaml_dump(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.yaml.dump", Mock(return_value=sentinel.yaml_dump),
    )


@pytest.fixture
def mock_bootstrap_logger(
    patch_datetime_now, patch_open, patch_os, patch_subprocess_popen, patch_yaml_dump
):
    return BootstrapLogger({}, sentinel.bootstrap_hash)


@pytest.mark.parametrize(
    "config, bootstrap_hash, expected_path, expected_open_calls, expected_os_calls, expected_subprocess_popen_calls",
    [
        (
            {"log folder": "a_log_folder"},
            sentinel.bootstrap_hash,
            Path("a_log_folder"),
            [call("a_log_folder/config.yml", mode="w")],
            {"exists": [call(Path("a_log_folder"))]},
            [call("git log -1", shell=True, stdout=subprocess.PIPE)],
        ),
        (
            {"log folder": "."},
            sentinel.bootstrap_hash,
            Path("."),
            [call("config.yml", mode="w")],
            {"exists": [call(Path("."))], "makedirs": [call(Path("."))]},
            [call("git log -1", shell=True, stdout=subprocess.PIPE)],
        ),
        (
            {},
            sentinel.bootstrap_hash,
            Path(
                "log_bootstrap/{date:%Y-%m-%d_%H:%M:%S}".format(date=MOCK_DATETIME_NOW)
            ),
            [
                call(
                    "log_bootstrap/{date:%Y-%m-%d_%H:%M:%S}/config.yml".format(
                        date=MOCK_DATETIME_NOW
                    ),
                    mode="w",
                ),
            ],
            {
                "exists": [
                    call(
                        Path(
                            "log_bootstrap/{date:%Y-%m-%d_%H:%M:%S}".format(
                                date=MOCK_DATETIME_NOW
                            )
                        )
                    )
                ]
            },
            [call("git log -1", shell=True, stdout=subprocess.PIPE)],
        ),
    ],
)
def test_bootstrap_logger_all_fields_set(
    patch_datetime_now,
    patch_open,
    patch_os,
    patch_subprocess_popen,
    patch_yaml_dump,
    config,
    bootstrap_hash,
    expected_path,
    expected_open_calls,
    expected_os_calls,
    expected_subprocess_popen_calls,
):
    logger = BootstrapLogger(config, bootstrap_hash)

    assert logger.train_losses == []
    assert logger.train_acc == []
    assert logger.val_losses == []
    assert logger.val_acc == []

    assert logger.folder_path == expected_path

    assert patch_open.mock_calls == expected_open_calls
    assert patch_os.makedirs.mock_calls == expected_os_calls.get("makedirs", [])
    assert patch_os.path.exists.mock_calls == expected_os_calls.get("exists", [])
    assert patch_subprocess_popen.mock_calls == expected_subprocess_popen_calls


@pytest.mark.parametrize(
    "logger, expected_fields",
    [
        (
            munch.munchify(
                {
                    "train_acc": sentinel.train_acc,
                    "train_losses": sentinel.train_losses,
                    "val_acc": sentinel.val_acc,
                    "val_losses": sentinel.val_losses,
                    "batch_losses": sentinel.batch_losses,
                    "batch_weights": sentinel.batch_weights,
                    "batch_gradients": sentinel.batch_gradients,
                },
            ),
            {
                "train_acc": [sentinel.train_acc],
                "train_losses": [sentinel.train_losses],
                "val_acc": [sentinel.val_acc],
                "val_losses": [sentinel.val_losses],
                "batch_losses": [sentinel.batch_losses],
                "batch_weights": [sentinel.batch_weights],
                "batch_gradients": [sentinel.batch_gradients],
            },
        ),
    ],
)
def test_process_iter(mock_bootstrap_logger, logger, expected_fields):
    mock_bootstrap_logger.process_iter(logger)

    for field_key, field_value in expected_fields.items():
        assert getattr(mock_bootstrap_logger, field_key) == field_value


@pytest.mark.parametrize(
    "folder_path, fields, expected_numpy_save_calls",
    [
        (
            Path("a_test_folder"),
            {
                "train_acc": sentinel.train_acc,
                "train_losses": sentinel.train_losses,
                "val_acc": sentinel.val_acc,
                "val_losses": sentinel.val_losses,
                "batch_losses": sentinel.batch_losses,
                "batch_weights": sentinel.batch_weights,
                "batch_gradients": sentinel.batch_gradients,
            },
            [
                call(Path("a_test_folder/train_accuracies.npy"), sentinel.train_acc),
                call(Path("a_test_folder/train_losses.npy"), sentinel.train_losses),
                call(Path("a_test_folder/val_accuracies.npy"), sentinel.val_acc),
                call(Path("a_test_folder/val_losses.npy"), sentinel.val_losses),
                call(Path("a_test_folder/batch_losses.npy"), sentinel.batch_losses),
                call(Path("a_test_folder/batch_weights.npy"), sentinel.batch_weights),
                call(
                    Path("a_test_folder/batch_gradients.npy"), sentinel.batch_gradients
                ),
            ],
        ),
        (
            Path("b_test_folder"),
            {
                "train_acc": sentinel.train_acc_1,
                "train_losses": sentinel.train_losses_1,
                "val_acc": sentinel.val_acc_1,
                "val_losses": sentinel.val_losses_1,
            },
            [
                call(Path("b_test_folder/train_accuracies.npy"), sentinel.train_acc_1),
                call(Path("b_test_folder/train_losses.npy"), sentinel.train_losses_1),
                call(Path("b_test_folder/val_accuracies.npy"), sentinel.val_acc_1),
                call(Path("b_test_folder/val_losses.npy"), sentinel.val_losses_1),
            ],
        ),
    ],
)
def test_process_end(
    mock_bootstrap_logger,
    patch_numpy_save,
    folder_path,
    fields,
    expected_numpy_save_calls,
):
    mock_bootstrap_logger.folder_path = folder_path
    for field_key, field_value in fields.items():
        setattr(mock_bootstrap_logger, field_key, field_value)

    mock_bootstrap_logger.process_end()

    assert patch_numpy_save.mock_calls == expected_numpy_save_calls
