from unittest.mock import sentinel

import pytest

from squid_helpers.loggers import AbstractLogger


@pytest.fixture
def mock_abstract_logger():
    return MockAbstractLogger()


class MockAbstractLogger(AbstractLogger):
    def process_update(self, running_loss, validation_loader):
        return sentinel.process_update

    def process_iter(self, y_truth, y_pred, running_loss, validation_loader):
        return sentinel.process_iter

    def process_end(self):
        return sentinel.process_end

    def process_test(self, y_truth, y_pred, running_loss):
        return sentinel.process_test


def test_process_update(mock_abstract_logger):
    result = mock_abstract_logger.process_update(
        sentinel.running_loss, sentinel.validation_loader,
    )

    assert result == sentinel.process_update


def test_process_iter(mock_abstract_logger):
    result = mock_abstract_logger.process_iter(
        sentinel.y_truth,
        sentinel.y_pred,
        sentinel.running_loss,
        sentinel.validation_loader,
    )

    assert result == sentinel.process_iter


def test_process_end(mock_abstract_logger):
    result = mock_abstract_logger.process_end()

    assert result == sentinel.process_end


def test_process_test(mock_abstract_logger):
    result = mock_abstract_logger.process_test(
        sentinel.y_truth, sentinel.y_pred, sentinel.running_loss,
    )

    assert result == sentinel.process_test
