from unittest.mock import sentinel

import pytest

from squid_helpers.loggers import AbstractBootstrapLogger


@pytest.fixture
def mock_abstract_bootstrap_logger():
    return MockAbstractBootstrapLogger()


class MockAbstractBootstrapLogger(AbstractBootstrapLogger):
    def process_iter(self, logger):
        return sentinel.process_iter

    def process_end(self):
        return sentinel.process_end


def test_process_iter(mock_abstract_bootstrap_logger):
    result = mock_abstract_bootstrap_logger.process_iter(sentinel.logger)

    assert result == sentinel.process_iter


def test_process_end(mock_abstract_bootstrap_logger):
    result = mock_abstract_bootstrap_logger.process_end()

    assert result == sentinel.process_end
