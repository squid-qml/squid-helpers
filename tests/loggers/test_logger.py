from datetime import datetime
from pathlib import Path
from unittest.mock import Mock, call, sentinel

import pytest

from squid_helpers.loggers import Logger

MOCK_DATETIME_NOW = datetime(year=2020, month=1, day=1)


@pytest.fixture
def patch_path():
    return "squid_helpers.loggers.logger"


@pytest.fixture
def patch_subprocess_popen(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.subprocess.Popen",
        Mock(
            return_value=Mock(
                stdout=Mock(
                    read=Mock(
                        return_value=Mock(
                            decode=Mock(return_value=sentinel.git_version)
                        )
                    )
                ),
            ),
        ),
    )


@pytest.fixture
def patch_os(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.os",
        Mock(
            path=Mock(exists=Mock(side_effect=lambda x: not str(x) == ".")),
            makedirs=Mock(return_value=sentinel.os_makedirs),
        ),
    )


@pytest.fixture
def patch_print(mocker, patch_path):
    return mocker.patch(f"{patch_path}.print", Mock(return_value=None),)


@pytest.fixture
def patch_yaml_dump(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.yaml.dump", Mock(return_value=sentinel.yaml_dump),
    )


@pytest.fixture
def patch_open(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.open",
        Mock(
            return_value=Mock(
                __enter__=Mock(
                    return_value=Mock(
                        writelines=Mock(return_value=sentinel.file_writelines)
                    )
                ),
                __exit__=Mock(return_value=None),
            ),
        ),
    )


@pytest.fixture
def patch_warnings_warn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.warnings.warn", Mock(return_value=sentinel.warning),
    )


@pytest.fixture
def patch_datetime_now(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.datetime", Mock(now=Mock(return_value=MOCK_DATETIME_NOW)),
    )


@pytest.fixture
def patch_accuracy_score(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.accuracy_score", Mock(side_effect=lambda x, y: (x, y)),
    )


@pytest.fixture
def patch_tqdm(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.tqdm", Mock(side_effect=lambda x, *args, **kwargs: x),
    )


@pytest.fixture
def patch_numpy_save(mocker, patch_path):
    return mocker.patch(f"{patch_path}.np.save", Mock(return_value=None),)


@pytest.fixture
def patch_torch_save(mocker, patch_path):
    return mocker.patch(f"{patch_path}.torch.save", Mock(return_value=None),)


@pytest.fixture
def patch_torch_argmax(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch.argmax",
        Mock(side_effect=lambda x, *args, **kwargs: to_torch_array(x)),
    )


@pytest.fixture
def mock_logger(
    patch_datetime_now, patch_open, patch_yaml_dump, patch_os, patch_subprocess_popen,
):
    return Logger(
        {"verbose": sentinel.verbose, "__DEBUG": sentinel.debug},
        sentinel.model,
        sentinel.criterion,
        "cpu",
    )


@pytest.fixture
def mock_model():
    return Mock(side_effect=lambda x: to_torch_array(x))


@pytest.fixture
def mock_criterion():
    return Mock(side_effect=lambda *args: Mock(item=Mock(return_value=10)))


# region: helpers


def to_torch_array(obj):
    return Mock(cpu=lambda: Mock(tolist=lambda: [obj]), to=Mock(return_value=obj),)


# end region: helpers


@pytest.mark.parametrize(
    "config, expected_fields, expected_os_calls, expected_open_calls, expected_subprocess_popen_calls, expected_warning_calls",
    [
        (
            {"verbose": sentinel.verbose, "__DEBUG": sentinel.debug},
            {},
            {
                "exists": [
                    call(
                        Path(
                            "log/{date:%Y-%m-%d_%H:%M:%S.%f}".format(
                                date=MOCK_DATETIME_NOW
                            )
                        )
                    )
                ],
            },
            [
                call(
                    "log/{date:%Y-%m-%d_%H:%M:%S.%f}/config.yml".format(
                        date=MOCK_DATETIME_NOW
                    ),
                    mode="w",
                ),
            ],
            [call("git log -1", shell=True, stdout=-1)],
            [],
        ),
        (
            {
                "verbose": sentinel.verbose,
                "__DEBUG": sentinel.debug,
                "log folder": ".",
                "model main": {"save best": True, "save best path": "../loggers"},
            },
            {
                "save_best": True,
                "save_best_path": Path("../loggers"),
                "folder_path": Path("."),
            },
            {"exists": [call(Path("."))], "makedirs": [call(Path("."))]},
            [call("config.yml", mode="w")],
            [call("git log -1", shell=True, stdout=-1)],
            [],
        ),
        (
            {
                "verbose": sentinel.verbose,
                "__DEBUG": sentinel.debug,
                "model main": {
                    "save best": True,
                    "save best path": "this_path_does_not_exist/a_model",
                },
            },
            {
                "save_best": True,
                "save_best_path": Path("this_path_does_not_exist/a_model"),
            },
            {
                "exists": [
                    call(
                        Path(
                            "log/{date:%Y-%m-%d_%H:%M:%S.%f}".format(
                                date=MOCK_DATETIME_NOW
                            )
                        )
                    )
                ],
            },
            [
                call(
                    "log/{date:%Y-%m-%d_%H:%M:%S.%f}/config.yml".format(
                        date=MOCK_DATETIME_NOW
                    ),
                    mode="w",
                ),
            ],
            [call("git log -1", shell=True, stdout=-1)],
            [
                call(
                    "Folder in which to save best model does not exist and "
                    "hence such saving will be ignored.\n"
                    "If you set a flag save best model, it still will be saved to:\n"
                    "log/2020-01-01_00:00:00.000000/best_model.pth"
                )
            ],
        ),
    ],
)
def test_logger_fields_are_set(
    patch_os,
    patch_open,
    patch_subprocess_popen,
    patch_warnings_warn,
    patch_yaml_dump,
    patch_datetime_now,
    config,
    expected_fields,
    expected_os_calls,
    expected_open_calls,
    expected_subprocess_popen_calls,
    expected_warning_calls,
):
    logger = Logger(config, sentinel.model, sentinel.criterion, sentinel.device)

    assert patch_os.path.exists.mock_calls == expected_os_calls.get("exists", [])
    assert patch_os.makedirs.mock_calls == expected_os_calls.get("makedirs", [])
    assert patch_open.mock_calls == expected_open_calls
    assert patch_subprocess_popen.mock_calls == expected_subprocess_popen_calls
    assert patch_warnings_warn.mock_calls == expected_warning_calls

    expected_fields["update"] = expected_fields.get("update", 0)
    expected_fields["iter"] = expected_fields.get("iter", 0)
    expected_fields["folder_path"] = expected_fields.get(
        "folder_path",
        Path("log/{date:%Y-%m-%d_%H:%M:%S.%f}".format(date=MOCK_DATETIME_NOW)),
    )
    expected_fields["verbose"] = expected_fields.get("verbose", config["verbose"])
    expected_fields["debug"] = expected_fields.get("debug", config["__DEBUG"])
    expected_fields["best_accuracy"] = expected_fields.get("best_accuracy", 0)
    expected_fields["save_best"] = expected_fields.get("save_best", False)
    expected_fields["save_best_path"] = expected_fields.get("save_best_path", None)
    expected_fields["train_losses"] = expected_fields.get("train_losses", [])
    expected_fields["train_acc"] = expected_fields.get("train_acc", [])
    expected_fields["val_losses"] = expected_fields.get("val_losses", [])
    expected_fields["val_acc"] = expected_fields.get("val_acc", [])
    expected_fields["model"] = expected_fields.get("model", sentinel.model)
    expected_fields["criterion"] = expected_fields.get("criterion", sentinel.criterion)
    expected_fields["device"] = sentinel.device
    for field_name, field_value in expected_fields.items():
        assert getattr(logger, field_name) == field_value

    # Assert Every Field checked
    logger_field_set = set(
        [
            x
            for x in dir(logger)
            if not x.startswith("__")
            and not x.startswith("_Logger")
            and not callable(getattr(logger, x))
        ]
    )
    expected_field_set = set(expected_fields.keys())
    assert logger_field_set.issubset(expected_field_set)


@pytest.mark.parametrize(
    "verbose, iteration, update, loss, expected_calls",
    [
        (0, 1, 2, 0.5, []),
        (1, 13, 22, 15, []),
        (2, 5, 1, -15, []),
        (3, 1, 2, 0.5, [call("Running loss. Epoch 1 Update 2: 0.5")],),
        (3, 1, 2, 0.5, [call("Running loss. Epoch 1 Update 2: 0.5")],),
        (4, 2, 1, 0.25, [call("Running loss. Epoch 2 Update 1: 0.25")],),
        (3, 0, 0, -2.0, [call("Running loss. Epoch 0 Update 0: -2.0")],),
        (
            3,
            123213,
            214214,
            0.25,
            [call("Running loss. Epoch 123213 Update 214214: 0.25")],
        ),
    ],
)
def test_print_update(
    mock_logger, patch_print, verbose, iteration, update, loss, expected_calls
):
    mock_logger.verbose = verbose
    mock_logger.iter = iteration
    mock_logger.update = update

    mock_logger._print_update(loss, None)

    assert patch_print.mock_calls == expected_calls


@pytest.mark.parametrize(
    "debug, update, loss, weights, gradients, expected_losses, expected_weights, expected_gradients",
    [
        (False, 2, None, None, None, None, None, None),
        (
            True,
            3,
            1.6,
            sentinel.weights,
            sentinel.gradients,
            [0.4],
            [[sentinel.weights]],
            [[sentinel.gradients]],
        ),
    ],
)
def test_process_update(
    mock_logger,
    debug,
    update,
    loss,
    weights,
    gradients,
    expected_losses,
    expected_weights,
    expected_gradients,
):
    patch_print_update = mock_logger._print_update = Mock(return_value=None)
    mock_logger.update = update
    mock_logger.debug = debug

    weights = Mock(
        data=Mock(numpy=Mock(return_value=Mock(copy=Mock(return_value=weights))))
    )
    gradients = Mock(
        grad=Mock(numpy=Mock(return_value=Mock(copy=Mock(return_value=gradients))))
    )
    mock_logger.model.parameters = Mock(side_effect=[[weights], [gradients]])

    mock_logger.process_update(loss, sentinel.validation_loader)

    assert mock_logger.update == update + 1

    assert patch_print_update.mock_calls == [call(loss, sentinel.validation_loader)]

    if expected_losses is None:
        assert "batch_losses" not in dir(mock_logger)
    else:
        assert mock_logger.batch_losses == expected_losses
    if expected_weights is None:
        assert "batch_weights" not in dir(mock_logger)
    else:
        assert mock_logger.batch_weights == expected_weights
    if expected_gradients is None:
        assert "batch_gradients" not in dir(mock_logger)
    else:
        assert mock_logger.batch_gradients == expected_gradients


@pytest.mark.parametrize(
    "verbose, iteration, train_loss, train_acc, val_loss, val_acc, expected_calls",
    [
        (0, 0, None, None, None, None, []),
        (
            1,
            0,
            [None, sentinel.not_called, sentinel.train_loss],
            [None, sentinel.train_acc],
            [sentinel.val_loss],
            [None, sentinel.not_called, sentinel.not_called, sentinel.val_acc],
            [
                call(""),
                call(f"Epoch 0 Loss: {sentinel.train_loss}"),
                call(f"Epoch 0 Accuracy: {sentinel.train_acc}"),
            ],
        ),
        (
            1,
            10,
            [None, sentinel.not_called, sentinel.train_loss],
            [None, sentinel.train_acc],
            [sentinel.val_loss],
            [None, sentinel.not_called, sentinel.not_called, sentinel.val_acc],
            [
                call(""),
                call(f"Epoch 10 Loss: {sentinel.train_loss}"),
                call(f"Epoch 10 Accuracy: {sentinel.train_acc}"),
            ],
        ),
        (
            2,
            0,
            [None, sentinel.not_called, sentinel.train_loss],
            [None, sentinel.train_acc],
            [sentinel.val_loss],
            [None, sentinel.not_called, sentinel.not_called, sentinel.val_acc],
            [
                call(""),
                call(f"Epoch 0 Loss: {sentinel.train_loss}"),
                call(f"Epoch 0 Accuracy: {sentinel.train_acc}"),
                call(f"Epoch 0 Validation Loss: {sentinel.val_loss}"),
                call(f"Epoch 0 Validation Accuracy: {sentinel.val_acc}"),
            ],
        ),
        (
            2,
            10,
            [None, sentinel.not_called, sentinel.train_loss],
            [None, sentinel.train_acc],
            [sentinel.val_loss],
            [None, sentinel.not_called, sentinel.not_called, sentinel.val_acc],
            [
                call(""),
                call(f"Epoch 10 Loss: {sentinel.train_loss}"),
                call(f"Epoch 10 Accuracy: {sentinel.train_acc}"),
                call(f"Epoch 10 Validation Loss: {sentinel.val_loss}"),
                call(f"Epoch 10 Validation Accuracy: {sentinel.val_acc}"),
            ],
        ),
    ],
)
def test_print_iter(
    mock_logger,
    patch_print,
    verbose,
    iteration,
    train_loss,
    train_acc,
    val_loss,
    val_acc,
    expected_calls,
):
    mock_logger.verbose = verbose
    mock_logger.iter = iteration

    mock_logger.train_losses = train_loss
    mock_logger.train_acc = train_acc
    mock_logger.val_losses = val_loss
    mock_logger.val_acc = val_acc

    mock_logger._print_iter()

    assert patch_print.mock_calls == expected_calls


@pytest.mark.parametrize(
    "input_truth, input_pred, loss, validation_loader, expected_train_losses, expected_train_acc, expected_train_y_pred, expected_train_y_truth, expected_val_losses, expected_val_acc, expected_val_y_pred, expected_val_y_truth",
    [
        (
            sentinel.input_truth,
            sentinel.input_pred,
            sentinel.loss,
            [],
            [sentinel.loss],
            [(sentinel.input_truth, sentinel.input_pred)],
            [sentinel.input_pred],
            [sentinel.input_truth],
            [0.0],
            [([], [])],
            [],
            [],
        ),
        (
            sentinel.input_truth,
            sentinel.input_pred,
            sentinel.loss,
            [(to_torch_array(sentinel.val_x_1), to_torch_array(sentinel.val_y_1))],
            [sentinel.loss],
            [(sentinel.input_truth, sentinel.input_pred)],
            [sentinel.input_pred],
            [sentinel.input_truth],
            [10.0],
            [([sentinel.val_y_1], [[sentinel.val_x_1]])],
            [[sentinel.val_x_1]],
            [sentinel.val_y_1],
        ),
    ],
)
def test_update_iter(
    mock_logger,
    mock_model,
    mock_criterion,
    patch_accuracy_score,
    patch_torch_argmax,
    patch_tqdm,
    input_truth,
    input_pred,
    loss,
    validation_loader,
    expected_train_losses,
    expected_train_acc,
    expected_train_y_pred,
    expected_train_y_truth,
    expected_val_losses,
    expected_val_acc,
    expected_val_y_pred,
    expected_val_y_truth,
):
    mock_logger.criterion = mock_criterion
    mock_logger.model = mock_model

    validation_loader = [(x, to_torch_array(y)) for x, y in validation_loader]

    mock_logger._update_iter(input_truth, input_pred, loss, validation_loader)

    assert mock_logger.train_losses == expected_train_losses
    assert mock_logger.train_acc == expected_train_acc

    assert mock_logger.train_y_pred == expected_train_y_pred
    assert mock_logger.train_y_truth == expected_train_y_truth

    assert mock_logger.val_losses == expected_val_losses
    assert len(mock_logger.val_acc) == len(expected_val_acc)
    assert all([len(x) == 2 for x in mock_logger.val_acc])
    for (r_0, r_1), (e_0, e_1) in zip(mock_logger.val_acc, expected_val_acc):
        assert len(r_0) == len(e_0)
        if len(r_0) != 0:
            assert r_0 == e_0
        assert len(r_1) == len(e_1)
        for sub_r, sub_e in zip(r_1, e_1):
            assert sub_r.cpu().tolist() == sub_e

    assert len(mock_logger.val_y_pred) == len(expected_val_y_pred)
    for r, e in zip(mock_logger.val_y_pred, expected_val_y_pred):
        assert r.cpu().tolist() == e
    assert len(mock_logger.val_y_truth) == len(expected_val_y_truth)
    if len(mock_logger.val_y_truth) != 0:
        assert mock_logger.val_y_truth == expected_val_y_truth


@pytest.mark.parametrize(
    "iteration, y_truth, y_pred, running_loss, validation_loader, expected_update_iter_calls",
    [
        (
            10,
            sentinel.y_truth,
            sentinel.y_pred,
            sentinel.loss,
            sentinel.val_loader,
            [
                call(
                    sentinel.y_truth,
                    sentinel.y_pred,
                    sentinel.loss,
                    sentinel.val_loader,
                ),
            ],
        ),
        (
            124,
            sentinel.y_truth_1,
            sentinel.y_pred_1,
            sentinel.loss_1,
            sentinel.val_loader_1,
            [
                call(
                    sentinel.y_truth_1,
                    sentinel.y_pred_1,
                    sentinel.loss_1,
                    sentinel.val_loader_1,
                ),
            ],
        ),
    ],
)
def test_process_iter(
    mock_logger,
    iteration,
    y_truth,
    y_pred,
    running_loss,
    validation_loader,
    expected_update_iter_calls,
):
    patch_update_iter = mock_logger._update_iter = Mock(return_value=None)
    mock_logger._print_iter = Mock(return_value=None)

    mock_logger.iter = iteration

    mock_logger.process_iter(y_truth, y_pred, running_loss, validation_loader)

    assert mock_logger.iter == iteration + 1
    assert patch_update_iter.mock_calls == expected_update_iter_calls


@pytest.mark.parametrize(
    "has_batch_losses, save_best, save_best_path, folder_path, expected_numpy_save_calls, expected_torch_save_calls",
    [
        (
            False,
            False,
            None,
            Path("a_test_folder"),
            [
                call(Path("a_test_folder/train_accuracies.npy"), sentinel.train_acc),
                call(Path("a_test_folder/train_losses.npy"), sentinel.train_losses),
                call(Path("a_test_folder/val_accuracies.npy"), sentinel.val_acc),
                call(Path("a_test_folder/val_losses.npy"), sentinel.val_losses),
            ],
            [call(sentinel.state_dict, Path("a_test_folder/last_model.pth"))],
        ),
        (
            True,
            True,
            None,
            Path("b_test_folder"),
            [
                call(Path("b_test_folder/train_accuracies.npy"), sentinel.train_acc),
                call(Path("b_test_folder/train_losses.npy"), sentinel.train_losses),
                call(Path("b_test_folder/val_accuracies.npy"), sentinel.val_acc),
                call(Path("b_test_folder/val_losses.npy"), sentinel.val_losses),
                call(
                    Path("b_test_folder/batch_train_losses.npy"), sentinel.batch_losses
                ),
                call(Path("b_test_folder/batch_weights.npy"), sentinel.batch_weights),
                call(
                    Path("b_test_folder/batch_gradients.npy"), sentinel.batch_gradients
                ),
            ],
            [
                call(sentinel.state_dict, Path("b_test_folder/last_model.pth")),
                call(sentinel.state_dict, Path("b_test_folder/best_model.pth")),
            ],
        ),
        (
            True,
            True,
            Path("best_path"),
            Path("c_test_folder"),
            [
                call(Path("c_test_folder/train_accuracies.npy"), sentinel.train_acc),
                call(Path("c_test_folder/train_losses.npy"), sentinel.train_losses),
                call(Path("c_test_folder/val_accuracies.npy"), sentinel.val_acc),
                call(Path("c_test_folder/val_losses.npy"), sentinel.val_losses),
                call(
                    Path("c_test_folder/batch_train_losses.npy"), sentinel.batch_losses
                ),
                call(Path("c_test_folder/batch_weights.npy"), sentinel.batch_weights),
                call(
                    Path("c_test_folder/batch_gradients.npy"), sentinel.batch_gradients
                ),
            ],
            [
                call(sentinel.state_dict, Path("c_test_folder/last_model.pth")),
                call(sentinel.state_dict, Path("c_test_folder/best_model.pth")),
                call(sentinel.state_dict, Path("best_path")),
            ],
        ),
    ],
)
def test_process_end(
    mock_logger,
    patch_numpy_save,
    patch_torch_save,
    has_batch_losses,
    save_best,
    save_best_path,
    folder_path,
    expected_numpy_save_calls,
    expected_torch_save_calls,
):
    mock_logger.train_acc = sentinel.train_acc
    mock_logger.train_losses = sentinel.train_losses
    mock_logger.val_acc = sentinel.val_acc
    mock_logger.val_losses = sentinel.val_losses
    if has_batch_losses:
        mock_logger.batch_losses = sentinel.batch_losses
        mock_logger.batch_weights = sentinel.batch_weights
        mock_logger.batch_gradients = sentinel.batch_gradients

    mock_logger.save_best = save_best
    mock_logger.save_best_path = save_best_path
    mock_logger.folder_path = folder_path
    mock_logger.model.state_dict = Mock(return_value=sentinel.state_dict)

    mock_logger.process_end()

    assert patch_numpy_save.mock_calls == expected_numpy_save_calls
    assert patch_torch_save.mock_calls == expected_torch_save_calls


@pytest.mark.parametrize(
    "folder_path, y_truth, y_pred, loss, expected_accuracy_score_calls, expected_numpy_save_calls, expected_open_calls, expected_file_write_line_calls",
    [
        (
            Path("test_folder_path"),
            sentinel.y_truth,
            sentinel.y_pred,
            sentinel.loss,
            [call(sentinel.y_truth, sentinel.y_pred)],
            [
                call(Path("test_folder_path/test_truth.npy"), sentinel.y_truth),
                call(Path("test_folder_path/test_predictions.npy"), sentinel.y_pred),
            ],
            [call(Path("test_folder_path/test_accuracy_loss.txt"))],
            [
                call(
                    [
                        f"Accuracy: {(sentinel.y_truth, sentinel.y_pred)}\n",
                        f"Loss: {sentinel.loss}\n",
                    ],
                ),
            ],
        ),
    ],
)
def test_process_test(
    mock_logger,
    patch_accuracy_score,
    patch_numpy_save,
    patch_open,
    folder_path,
    y_truth,
    y_pred,
    loss,
    expected_accuracy_score_calls,
    expected_numpy_save_calls,
    expected_open_calls,
    expected_file_write_line_calls,
):
    mock_logger.folder_path = folder_path

    mock_logger.process_test(y_truth, y_pred, loss)

    assert patch_accuracy_score.mock_calls == expected_accuracy_score_calls
    assert patch_numpy_save.mock_calls == expected_numpy_save_calls
    assert patch_open.mock_calls[1:] == expected_open_calls
    assert (
        patch_open.return_value.__enter__.return_value.writelines.mock_calls
        == expected_file_write_line_calls
    )
