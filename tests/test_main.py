from unittest.mock import Mock, call, sentinel

import pytest

import main
from main import test as main_test
from main import train as main_train


@pytest.fixture
def patch_path():
    return "main"


@pytest.fixture
def patch_bootstrap(mocker, patch_path):
    return mocker.patch(f"{patch_path}.bootstrap", Mock(return_value=None))


@pytest.fixture
def patch_train(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.train", Mock(return_value=None, __code__=main_train.__code__)
    )


@pytest.fixture
def patch_test(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.test", Mock(return_value=None, __code__=main_test.__code__)
    )


@pytest.fixture
def patch_random_randint(mocker, patch_path):
    return mocker.patch(f"{patch_path}.random.randint", Mock(return_value=2))


@pytest.fixture
def patch_joblib_delayed(mocker, patch_path):
    return mocker.patch(f"{patch_path}.delayed", Mock(return_value=sentinel.delayed))


@pytest.fixture
def patch_tqdm(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.tqdm", Mock(side_effect=lambda x, *args, **kwargs: x),
    )


@pytest.fixture
def patch_init_from_config_path(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.init_from_config_path",
        Mock(
            return_value=(
                sentinel.config,
                sentinel.model,
                sentinel.optimizer,
                sentinel.loss,
                sentinel.train_data,
                sentinel.validation_data,
                sentinel.test_data,
                sentinel.train_loader,
                sentinel.validation_loader,
                sentinel.test_loader,
                sentinel.epochs,
                sentinel.batch_size,
                sentinel.device,
                sentinel.logger,
                sentinel.verbose,
            ),
        ),
    )


@pytest.fixture
def patch_bootstrap_logger(mocker, patch_path, mock_bootstrap_logger):
    return mocker.patch(
        f"{patch_path}.BootstrapLogger", Mock(return_value=mock_bootstrap_logger)
    )


@pytest.fixture
def patch_logger(mocker, patch_path, mock_logger):
    return mocker.patch(f"{patch_path}.Logger", Mock(return_value=mock_logger))


@pytest.fixture
def patch_open(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.open",
        Mock(
            return_value=Mock(
                __enter__=Mock(
                    return_value=Mock(
                        writelines=Mock(return_value=sentinel.file_writelines)
                    )
                ),
                __exit__=Mock(return_value=None),
            ),
        ),
    )


@pytest.fixture
def patch_yaml_safe_load(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.yaml.safe_load", Mock(return_value=sentinel.yaml_safe_load),
    )


@pytest.fixture
def patch_torch(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch",
        Mock(
            argmax=Mock(side_effect=lambda tensor, *args, **kwargs: tensor),
            no_grad=Mock(
                __enter__=lambda *args, **kwargs: None,
                __exit__=lambda *args, **kwargs: None,
            ),
        ),
    )


@pytest.fixture
def patch_print(mocker, patch_path):
    return mocker.patch(f"{patch_path}.print", Mock(return_value=None),)


# region: mocks


@pytest.fixture
def mock_bootstrap_logger():
    return Mock(
        process_end=Mock(return_value=None), process_iter=Mock(return_value=None),
    )


@pytest.fixture
def mock_logger():
    return Mock(
        process_end=Mock(return_value=None),
        process_iter=Mock(return_value=None),
        process_test=Mock(return_value=None),
    )


@pytest.fixture
def mock_optimizer():
    return Mock(zero_grad=Mock(return_value=None), step=Mock(return_value=None),)


# end region: mocks


# region: helpers


def array_to_mock_tensor(standard_array):
    return Mock(
        to=lambda *args, **kwargs: standard_array,
        detach=lambda: Mock(cpu=lambda: Mock(tolist=lambda: standard_array)),
        view=lambda *args, **kwargs: Mock(
            to=lambda *args, **kwargs: Mock(
                detach=lambda: Mock(cpu=lambda: Mock(tolist=lambda: standard_array))
            )
        ),
    )


def arrays_to_loader(inputs, targets):
    return [
        (array_to_mock_tensor(inp), array_to_mock_tensor(target))
        for inp, target in zip(inputs, targets)
    ]


def losses_to_criterion(losses):
    return Mock(side_effect=[Mock(item=Mock(return_value=loss)) for loss in losses],)


# end region: helpers


@pytest.mark.parametrize(
    "configs, expected_train_calls, expected_test_calls, expected_bootstrap_calls,",
    [
        (
            [
                {"BOOTSTRAP": True, "train": True, "test": True},
                {"BOOTSTRAP": False, "train": True, "test": True},
                {"BOOTSTRAP": False, "train": False, "test": True},
                {"train": True, "test": False},
            ],
            [
                call(
                    model=sentinel.model,
                    optimizer=sentinel.optimizer,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    train_loader=sentinel.train_loader,
                    validation_loader=sentinel.validation_loader,
                    epochs=sentinel.epochs,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
                call(
                    model=sentinel.model,
                    optimizer=sentinel.optimizer,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    train_loader=sentinel.train_loader,
                    validation_loader=sentinel.validation_loader,
                    epochs=sentinel.epochs,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
            ],
            [
                call(
                    model=sentinel.model,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    test_loader=sentinel.test_loader,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
                call(
                    model=sentinel.model,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    test_loader=sentinel.test_loader,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
            ],
            [
                call(
                    sentinel.config_path_0,
                    {"BOOTSTRAP": True, "train": True, "test": True},
                )
            ],
        ),
    ],
)
def test_main(
    mocker,
    patch_path,
    patch_bootstrap,
    patch_train,
    patch_test,
    configs,
    expected_train_calls,
    expected_test_calls,
    expected_bootstrap_calls,
):
    mocker.patch(
        f"{patch_path}.read_configs",
        Mock(
            return_value=[
                getattr(sentinel, f"config_path_{x}") for x in range(len(configs))
            ],
        ),
    )
    mocker.patch(
        f"{patch_path}.init_from_config_path",
        Mock(
            side_effect=[
                (
                    config,
                    {
                        "model": sentinel.model,
                        "optimizer": sentinel.optimizer,
                        "criterion": sentinel.loss,
                        "train_data": sentinel.train_data,
                        "validation_data": sentinel.validation_data,
                        "test_data": sentinel.test_data,
                        "train_loader": sentinel.train_loader,
                        "validation_loader": sentinel.validation_loader,
                        "test_loader": sentinel.test_loader,
                        "epochs": sentinel.epochs,
                        "batch_size": sentinel.batch_size,
                        "device": sentinel.device,
                        "logger": sentinel.logger,
                        "verbose": sentinel.verbose,
                    },
                )
                for config in configs
            ],
        ),
    )
    main.main()

    assert patch_train.mock_calls == expected_train_calls
    assert patch_test.mock_calls == expected_test_calls
    assert patch_bootstrap.mock_calls == expected_bootstrap_calls


@pytest.mark.parametrize(
    "config, bootstrap_idx, expected_yaml_safe_load_num_calls, expected_train_calls, expected_test_calls, expected_random_seed, expected_log_folder_path",
    [
        (
            {
                "log folder": "a_folder",
                "__RANDOM_SEED": 7,
                "train": True,
                "test": False,
            },
            54,
            0,
            [
                call(
                    model=sentinel.model,
                    optimizer=sentinel.optimizer,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    train_loader=sentinel.train_loader,
                    validation_loader=sentinel.validation_loader,
                    epochs=sentinel.epochs,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
            ],
            [],
            61,
            "a_folder/log/54",
        ),
        (
            "a_path_to_a_log",
            2,
            1,
            [
                call(
                    model=sentinel.model,
                    optimizer=sentinel.optimizer,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    train_loader=sentinel.train_loader,
                    validation_loader=sentinel.validation_loader,
                    epochs=sentinel.epochs,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
            ],
            [
                call(
                    model=sentinel.model,
                    criterion=sentinel.loss,
                    device=sentinel.device,
                    test_loader=sentinel.test_loader,
                    logger=sentinel.logger,
                    verbose=sentinel.verbose,
                ),
            ],
            None,
            "a_path_to_a_log/log/2",
        ),
    ],
)
def test_single_run(
    mocker,
    patch_path,
    patch_train,
    patch_test,
    patch_open,
    config,
    bootstrap_idx,
    expected_yaml_safe_load_num_calls,
    expected_train_calls,
    expected_test_calls,
    expected_random_seed,
    expected_log_folder_path,
):
    patch_init_from_config_path = mocker.patch(
        f"{patch_path}.init_from_config",
        Mock(
            side_effect=lambda c, *args, **kwargs: (
                c,
                {
                    "model": sentinel.model,
                    "optimizer": sentinel.optimizer,
                    "criterion": sentinel.loss,
                    "train_data": sentinel.train_data,
                    "validation_data": sentinel.validation_data,
                    "test_data": sentinel.test_data,
                    "train_loader": sentinel.train_loader,
                    "validation_loader": sentinel.validation_loader,
                    "test_loader": sentinel.test_loader,
                    "epochs": sentinel.epochs,
                    "batch_size": sentinel.batch_size,
                    "device": sentinel.device,
                    "logger": sentinel.logger,
                    "verbose": sentinel.verbose,
                },
            ),
        ),
    )

    patch_yaml_safe_load = mocker.patch(
        f"{patch_path}.yaml.safe_load",
        Mock(return_value={"train": True, "test": True, "log folder": config},),
    )

    main._single_run(config, sentinel.bootstrap_hash, bootstrap_idx)

    assert patch_train.mock_calls == expected_train_calls
    assert patch_test.mock_calls == expected_test_calls
    assert len(patch_yaml_safe_load.mock_calls) == expected_yaml_safe_load_num_calls
    assert (
        patch_init_from_config_path.mock_calls[0][1][0].get("__RANDOM_SEED", None)
        == expected_random_seed
    )
    assert (
        patch_init_from_config_path.mock_calls[0][1][0].get("log folder", None)
        == expected_log_folder_path
    )


@pytest.mark.parametrize(
    "config_path, config, results, expected_error, expected_process_iter_calls, expected_parallel_call",
    [
        (sentinel.config_path, {}, [], True, [], False,),
        (
            sentinel.config_path,
            {"BOOTSTRAP RUNS": 10, "BOOTSTRAP JOBS": 2},
            [sentinel.result_1, sentinel.result_2, sentinel.result_3],
            False,
            [
                call(sentinel.result_1),
                call(sentinel.result_2),
                call(sentinel.result_3),
            ],
            True,
        ),
        (
            sentinel.config_path,
            {"BOOTSTRAP RUNS": 10},
            [sentinel.result_4, sentinel.result_5, sentinel.result_6],
            False,
            [
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
                call(None),
            ],
            False,
        ),
    ],
)
def test_bootstrap(
    mocker,
    patch_path,
    patch_bootstrap_logger,
    patch_joblib_delayed,
    patch_random_randint,
    patch_open,
    mock_bootstrap_logger,
    config_path,
    config,
    results,
    expected_error,
    expected_process_iter_calls,
    expected_parallel_call,
):
    patch_parallel = mocker.patch(
        f"{patch_path}.Parallel", Mock(return_value=Mock(return_value=results))
    )
    mocker.patch(
        f"{patch_path}._single_run", Mock(return_value=None),
    )
    mocker.patch(
        f"{patch_path}.yaml.safe_load", Mock(return_value=config),
    )

    try:
        main.bootstrap(config_path, config)
        assert not expected_error
    except ValueError as e:
        if not expected_error:
            raise e
        else:
            return
    assert mock_bootstrap_logger.process_iter.mock_calls == expected_process_iter_calls
    assert (len(patch_parallel.mock_calls) > 0) == expected_parallel_call


@pytest.mark.parametrize(
    "losses, predictions, labels, epochs, logger, expected_process_iter_calls",
    [
        (
            [0.5, 0.2, 0.1, 0.2],
            [[sentinel.prediction]] * 4,
            [[sentinel.label]] * 4,
            1,
            None,
            [],
        ),
        (
            [0.5, 0.2, 0.1, 0.2],
            [[sentinel.prediction]] * 4,
            [[sentinel.label]] * 4,
            1,
            "mock_logger",
            [
                call(
                    [sentinel.label] * 4,
                    [sentinel.prediction] * 4,
                    0.25,
                    sentinel.validation_loader,
                ),
            ],
        ),
        (
            [0.5, 0.2, 0.1, 0.2, 0.1, 0.0, 0.1, 0.0],
            [[getattr(sentinel, f"prediction_{i}")] for i in range(2 * 4)],
            [[getattr(sentinel, f"label_{i}")] for i in range(4)],
            2,
            "mock_logger",
            [
                call(
                    [
                        sentinel.label_0,
                        sentinel.label_1,
                        sentinel.label_2,
                        sentinel.label_3,
                    ],
                    [
                        sentinel.prediction_0,
                        sentinel.prediction_1,
                        sentinel.prediction_2,
                        sentinel.prediction_3,
                    ],
                    0.25,
                    sentinel.validation_loader,
                ),
                call(
                    [
                        sentinel.label_0,
                        sentinel.label_1,
                        sentinel.label_2,
                        sentinel.label_3,
                    ],
                    [
                        sentinel.prediction_4,
                        sentinel.prediction_5,
                        sentinel.prediction_6,
                        sentinel.prediction_7,
                    ],
                    0.05,
                    sentinel.validation_loader,
                ),
            ],
        ),
    ],
)
def test_train(
    patch_tqdm,
    patch_print,
    patch_torch,
    mock_optimizer,
    mock_logger,
    losses,
    predictions,
    labels,
    epochs,
    logger,
    expected_process_iter_calls,
):
    logger = mock_logger if logger is not None else None
    model = Mock(
        side_effect=[array_to_mock_tensor(prediction) for prediction in predictions],
        backward=Mock(return_value=None),
    )
    model_with_device = Mock(to=Mock(return_value=model))

    criterion = losses_to_criterion(losses)

    main.train(
        model=model_with_device,
        optimizer=mock_optimizer,
        criterion=criterion,
        device=sentinel.device,
        train_loader=arrays_to_loader(
            [sentinel.input for _ in range(len(labels))], labels,
        ),
        validation_loader=sentinel.validation_loader,
        epochs=epochs,
        logger=logger,
        verbose=sentinel.verbose,
    )

    assert (
        len(mock_optimizer.zero_grad.mock_calls)
        == len(mock_optimizer.step.mock_calls)
        == len(criterion.mock_calls)
        == len(model.backward.mock_calls)
        == len(losses)
    )
    if logger is not None:
        assert len(mock_logger.process_iter.mock_calls) == epochs
        assert len(mock_logger.process_update.mock_calls) == len(losses)
        assert logger.process_iter.mock_calls == expected_process_iter_calls


@pytest.mark.parametrize(
    "losses, predictions, labels, logger, expected_process_test_calls",
    [
        (
            [0.5, 0.2, 0.1, 0.2],
            [[sentinel.prediction]] * 4,
            [[sentinel.label]] * 4,
            None,
            [],
        ),
        (
            [0.5, 0.2, 0.1, 0.2],
            [[sentinel.prediction]] * 4,
            [[sentinel.label]] * 4,
            "mock_logger",
            [call([sentinel.label] * 4, [sentinel.prediction] * 4, 0.25)],
        ),
    ],
)
def test_test(
    patch_tqdm,
    patch_torch,
    mock_logger,
    losses,
    predictions,
    labels,
    logger,
    expected_process_test_calls,
):
    logger = mock_logger if logger is not None else None
    model = Mock(
        side_effect=[array_to_mock_tensor(prediction) for prediction in predictions],
        backward=Mock(return_value=None),
    )
    model_with_device = Mock(to=Mock(return_value=model))

    criterion = losses_to_criterion(losses)

    main.test(
        model=model_with_device,
        criterion=criterion,
        device=sentinel.device,
        test_loader=arrays_to_loader(
            [sentinel.input for _ in range(len(labels))], labels,
        ),
        logger=logger,
        verbose=sentinel.verbose,
    )

    assert len(criterion.mock_calls) == len(model.mock_calls) == len(losses)

    if logger is not None:
        assert logger.process_test.mock_calls == expected_process_test_calls


def test_init(mocker, patch_path):
    mocker.patch(f"{patch_path}.__name__", "__not_main__")
    main_patch = mocker.patch(f"{patch_path}.main", Mock(return_value=None))
    main.init()
    assert main_patch.mock_calls == []
    mocker.patch(f"{patch_path}.__name__", "__main__")
    main.init()
    assert main_patch.mock_calls == [call()]
