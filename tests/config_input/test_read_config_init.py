from typing import Any, List
from unittest.mock import Mock, call, sentinel

import numpy as np
import pytest
import torch
from squid.models import ClassicalModel, QuantumModel

from squid_helpers.config_input import read_config_init


@pytest.fixture
def patch_path():
    return "squid_helpers.config_input.read_config_init"


@pytest.fixture
def patch_open(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.open",
        Mock(
            return_value=Mock(
                __enter__=Mock(
                    return_value=Mock(
                        writelines=Mock(return_value=sentinel.file_writelines)
                    )
                ),
                __exit__=Mock(return_value=None),
            ),
        ),
    )


@pytest.fixture
def patch_print(mocker, patch_path):
    return mocker.patch(f"{patch_path}.print", Mock(return_value=None),)


@pytest.fixture
def patch_yaml_safe_load(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.yaml.safe_load", Mock(return_value=sentinel.yaml_safe_load),
    )


@pytest.fixture
def patch_os(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.os",
        Mock(
            path=Mock(
                exists=Mock(side_effect=lambda x: not str(x) == "."),
                isdir=Mock(side_effect=lambda x: str(x) == "dir"),
                isfile=Mock(return_value=True),
            ),
            makedirs=Mock(return_value=sentinel.os_makedirs),
        ),
    )


@pytest.fixture
def patch_Path(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.Path",
        Mock(
            return_value=Mock(
                rglob=lambda s: [
                    f"yml_file_{i}" if s == "*.yml" else f"yaml_file_{i}"
                    for i in range(2)
                ],
            )
        ),
    )


@pytest.fixture
def patch_warnings_warn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.warnings.warn", Mock(return_value=sentinel.warning),
    )


@pytest.fixture
def patch_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.random.getstate", Mock(return_value=sentinel.random_state),
        ),
        mocker.patch(
            f"{patch_path}.random.seed", Mock(return_value=sentinel.random_state),
        ),
        mocker.patch(
            f"{patch_path}.random.setstate", Mock(return_value=sentinel.random_state),
        ),
    )


@pytest.fixture
def patch_np_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.np.random.get_state",
            Mock(return_value=sentinel.np_random_state),
        ),
        mocker.patch(
            f"{patch_path}.np.random.seed", Mock(return_value=sentinel.np_random_state),
        ),
        mocker.patch(
            f"{patch_path}.np.random.set_state",
            Mock(return_value=sentinel.np_random_state),
        ),
    )


@pytest.fixture
def patch_torch_random(mocker, patch_path):
    return (
        mocker.patch(
            f"{patch_path}.torch.random.get_rng_state",
            Mock(return_value=sentinel.torch_random_state),
        ),
        mocker.patch(
            f"{patch_path}.torch.random.manual_seed",
            Mock(return_value=sentinel.torch_random_state),
        ),
        mocker.patch(
            f"{patch_path}.torch.random.set_rng_state",
            Mock(return_value=sentinel.torch_random_state),
        ),
    )


@pytest.fixture
def patch_torch_cuda_is_available_false(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch.cuda.is_available", Mock(return_value=False),
    )


@pytest.fixture
def patch_torch_cuda_is_available_true(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch.cuda",
        Mock(
            is_available=Mock(return_value=True), current_device=Mock(return_value=0),
        ),
    )


@pytest.fixture
def patch_torch_load(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.torch.load",
        Mock(
            return_value={
                "model1.layer1": sentinel.layer11,
                "model1.layer2": sentinel.layer12,
                "model1.layer3": sentinel.layer13,
                "model2.layer1": sentinel.layer21,
                "model2.layer2": sentinel.layer22,
                "model2.layer3": sentinel.layer23,
                "model3.layer1": sentinel.layer31,
                "model3.layer2": sentinel.layer32,
                "model3.layer3": sentinel.layer33,
            },
        ),
    )


@pytest.fixture
def patch_data_loader(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.DataLoader",
        Mock(
            side_effect=[
                sentinel.train_loader,
                sentinel.validation_loader,
                sentinel.test_loader,
            ]
        ),
    )


@pytest.fixture
def patch_custom_dataset(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.CustomDataset",
        Mock(
            side_effect=[
                sentinel.train_data,
                sentinel.validation_data,
                sentinel.test_data,
            ]
        ),
    )


@pytest.fixture
def patch_model_from_config(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.model_from_config",
        Mock(
            return_value=Mock(
                model1=Mock(__class__=Mock(__name__=sentinel.model1)),
                model2=Mock(__class__=Mock(__name__=sentinel.model2)),
                model3=Mock(__class__=Mock(__name__=sentinel.model3)),
            )
        ),
    )


@pytest.fixture
def patch_get_opt_loss(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.get_loss_opt",
        Mock(
            return_value=(
                sentinel.optimizer,
                sentinel.optimizer_type,
                sentinel.loss,
                sentinel.loss_type,
            )
        ),
    )


@pytest.fixture
def patch_load_data(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.load_data",
        Mock(
            return_value=(
                (
                    (sentinel.train_raw_data_inputs, sentinel.train_raw_data_labels),
                    (
                        sentinel.validation_raw_data_inputs,
                        sentinel.validation_raw_data_labels,
                    ),
                    (sentinel.test_raw_data_inputs, sentinel.test_raw_data_labels),
                ),
                sentinel.dataset_type,
            ),
        ),
    )


@pytest.fixture
def patch_logger(mocker, patch_path):
    return mocker.patch(f"{patch_path}.Logger", Mock(return_value=sentinel.logger),)


@pytest.fixture
def patch_model_vqc(mocker, patch_path):
    return mocker.patch(f"{patch_path}.VQC", Mock(return_value=sentinel.VQC))


@pytest.fixture
def patch_model_conv_feed_forward(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.ConvFeedForward", Mock(return_value=sentinel.ConvFeedForward),
    )


@pytest.fixture
def patch_model_feed_forward(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.FeedForward", Mock(return_value=sentinel.FeedForward),
    )


@pytest.fixture
def patch_model_select_single_output(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.SelectSingleOutput",
        Mock(return_value=sentinel.SelectSingleOutput),
    )


@pytest.fixture
def sentinel_mock_quantum_model_subclass():
    return SentinelMockQuantumModel()


@pytest.fixture
def sentinel_mock_classical_model_subclass():
    return SentinelMockClassicalModel()


@pytest.fixture
def simple_mock_quantum_model_subclass():
    return SimpleMockQuantumModel()


@pytest.fixture
def simple_mock_classical_model_subclass():
    return SimpleMockClassicalModel()


# region: helpers


class SentinelMockQuantumModel(QuantumModel):
    def forward(self, x):
        return sentinel.quantum_forward

    def backward(self, x):
        return sentinel.quantum_backward


class SentinelMockClassicalModel(ClassicalModel):
    devices: List[Any] = []

    def forward(self, x):
        return sentinel.classical_forward

    def to(self, device):
        self.devices.append(device)
        return self


class SimpleMockQuantumModel(QuantumModel):
    def clip(self, x):
        return x / 2

    def forward(self, x):
        return x

    def backward(self, x):
        return x

    load_state_dict = Mock(side_effect=lambda x: x)


class SimpleMockClassicalModel(ClassicalModel):
    def forward(self, x):
        return x

    load_state_dict = Mock(side_effect=lambda x: x)


def list_to_parameters(param_list):
    return Mock(return_value=[Mock(value=x, requires_grad=y) for x, y in param_list])


def get_model(model_type: str):
    if model_type.lower() in [
        "simplemockquantummodel",
        "simple_mock_quantum_model",
        "quantum",
    ]:
        return SimpleMockQuantumModel()
    else:
        return SimpleMockClassicalModel()


def raise_value_error():
    raise ValueError()


# end region: helpers
# region: tests


@pytest.mark.parametrize(
    "config_file_paths, expected_result, expected_print_calls",
    [
        (["a_path", "a_path.yml"], ["a_path.yml"], [],),
        (["dir"], ["yaml_file_0", "yaml_file_1", "yml_file_0", "yml_file_1"], [],),
        (["."], [], [call("Path not found. Skipping .")],),
    ],
)
def test_read_configs(
    mocker,
    patch_path,
    patch_print,
    patch_os,
    patch_Path,
    config_file_paths,
    expected_result,
    expected_print_calls,
):
    mocker.patch(
        f"{patch_path}.argparse",
        Mock(
            ArgumentParser=Mock(
                return_value=Mock(
                    add_argument=Mock(return_value=None),
                    parse_args=Mock(
                        return_value=Mock(config_file_paths=config_file_paths)
                    ),
                ),
            ),
        ),
    )

    result = read_config_init.read_configs()

    assert list(result) == expected_result
    assert patch_print.mock_calls == expected_print_calls


@pytest.mark.parametrize(
    "config_path, expected_print_calls, expected_init_from_config_calls",
    [
        (".", [call("Path not found. Skipping .")], [],),
        ("a_config", [], [call(sentinel.yaml_safe_load, sentinel.bootstrap_hash)],),
    ],
)
def test_init_from_config_path(
    mocker,
    patch_path,
    patch_print,
    patch_os,
    patch_open,
    patch_yaml_safe_load,
    config_path,
    expected_print_calls,
    expected_init_from_config_calls,
):
    patch_init_from_config = mocker.patch(
        f"{patch_path}.init_from_config", Mock(return_value=sentinel.result),
    )

    read_config_init.init_from_config_path(config_path, sentinel.bootstrap_hash)

    assert patch_init_from_config.mock_calls == expected_init_from_config_calls
    assert patch_print.mock_calls == expected_print_calls


@pytest.mark.parametrize(
    "config, bootstrap_hash, expected_output_config, expected_output, expected_error",
    [
        (
            {
                "model 1": {"type": sentinel.model1_type},
                "model 2": {"type": sentinel.model2_type},
                "model 3": {"type": sentinel.model3_type},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
            },
            None,
            {
                "epochs": sentinel.epochs,
                "verbose": sentinel.verbose,
                "batch size": sentinel.batch_size,
                "train": True,
                "test": False,
                "__DEBUG": sentinel.debug,
                "model 1": {"type": sentinel.model1},
                "model 2": {"type": sentinel.model2},
                "model 3": {"type": sentinel.model3},
                "dataset": {
                    "type": sentinel.dataset_type,
                    "args": {
                        "dataset_arg_key": sentinel.dataset_arg_val,
                        "debug": sentinel.debug,
                    },
                },
            },
            {
                "optimizer": sentinel.optimizer,
                "criterion": sentinel.loss,
                "train_data": sentinel.train_data,
                "val_data": sentinel.validation_data,
                "test_data": sentinel.test_data,
                "train_loader": sentinel.train_loader,
                "validation_loader": sentinel.validation_loader,
                "test_loader": sentinel.test_loader,
                "epochs": sentinel.epochs,
                "batch_size": sentinel.batch_size,
                "device": "cpu",
                "logger": sentinel.logger,
                "verbose": sentinel.verbose,
            },
            False,
        ),
        (
            {
                "model 1": {"type": sentinel.model1_type, "args": {}},
                "model 2": {"type": sentinel.model2_type, "args": {}},
                "model 3": {"type": sentinel.model3_type, "args": {}},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
                "train": False,
                "test": True,
                "__RANDOM_SEED": 10,
            },
            sentinel.bootstrap_hash,
            {
                "epochs": sentinel.epochs,
                "verbose": sentinel.verbose,
                "batch size": sentinel.batch_size,
                "train": False,
                "test": True,
                "__DEBUG": sentinel.debug,
                "__RANDOM_SEED": 10,
                "BOOTSTRAP HASH": sentinel.bootstrap_hash,
                "model 1": {
                    "type": sentinel.model1,
                    "args": {"debug": sentinel.debug, "random_seed": 10},
                },
                "model 2": {
                    "type": sentinel.model2,
                    "args": {"debug": sentinel.debug, "random_seed": 11},
                },
                "model 3": {
                    "type": sentinel.model3,
                    "args": {"debug": sentinel.debug, "random_seed": 12},
                },
                "dataset": {
                    "type": sentinel.dataset_type,
                    "args": {
                        "dataset_arg_key": sentinel.dataset_arg_val,
                        "debug": sentinel.debug,
                        "random_seed": 13,
                    },
                },
            },
            {
                "optimizer": sentinel.optimizer,
                "criterion": sentinel.loss,
                "train_data": sentinel.train_data,
                "val_data": sentinel.validation_data,
                "test_data": sentinel.test_data,
                "train_loader": sentinel.train_loader,
                "validation_loader": sentinel.validation_loader,
                "test_loader": sentinel.test_loader,
                "epochs": sentinel.epochs,
                "batch_size": sentinel.batch_size,
                "device": "cpu",
                "logger": sentinel.logger,
                "verbose": sentinel.verbose,
            },
            False,
        ),
        (
            {
                "model 1": {"type": sentinel.model1_type},
                "model 2": {"type": sentinel.model2_type},
                "model 3": {"type": sentinel.model3_type},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
                "train": False,
                "test": False,
            },
            None,
            None,
            None,
            True,
        ),
    ],
)
def test_init_from_config(
    patch_custom_dataset,
    patch_data_loader,
    patch_get_opt_loss,
    patch_load_data,
    patch_logger,
    patch_model_from_config,
    patch_torch_cuda_is_available_false,
    config,
    bootstrap_hash,
    expected_output_config,
    expected_output,
    expected_error,
):
    processed_config = {
        "epochs": config.get("epochs", sentinel.epochs),
        "batch size": config.get("batch size", sentinel.batch_size),
        "verbose": config.get("verbose", sentinel.verbose),
        "__DEBUG": config.get("__DEBUG", sentinel.debug),
    }

    for k, v in config.items():
        processed_config[k] = v

    try:
        result = read_config_init.init_from_config(processed_config, bootstrap_hash)
        assert not expected_error
    except AssertionError as e:
        if not expected_error:
            raise e
        else:
            return

    result_config, result_kwarg = result

    del result_kwarg["model"]

    assert result_kwarg == expected_output
    assert result_config == expected_output_config


@pytest.mark.parametrize(
    "config, bootstrap_hash, expected_output_config, expected_output, expected_error",
    [
        (
            {
                "model 1": {"type": sentinel.model1_type},
                "model 2": {"type": sentinel.model2_type},
                "model 3": {"type": sentinel.model3_type},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
            },
            None,
            {
                "epochs": sentinel.epochs,
                "verbose": sentinel.verbose,
                "batch size": sentinel.batch_size,
                "train": True,
                "test": False,
                "__DEBUG": sentinel.debug,
                "model 1": {"type": sentinel.model1},
                "model 2": {"type": sentinel.model2},
                "model 3": {"type": sentinel.model3},
                "dataset": {
                    "type": sentinel.dataset_type,
                    "args": {
                        "dataset_arg_key": sentinel.dataset_arg_val,
                        "debug": sentinel.debug,
                    },
                },
            },
            {
                "optimizer": sentinel.optimizer,
                "criterion": sentinel.loss,
                "train_data": sentinel.train_data,
                "val_data": sentinel.validation_data,
                "test_data": sentinel.test_data,
                "train_loader": sentinel.train_loader,
                "validation_loader": sentinel.validation_loader,
                "test_loader": sentinel.test_loader,
                "epochs": sentinel.epochs,
                "batch_size": sentinel.batch_size,
                "device": 0,
                "logger": sentinel.logger,
                "verbose": sentinel.verbose,
            },
            False,
        ),
        (
            {
                "model 1": {"type": sentinel.model1_type, "args": {}},
                "model 2": {"type": sentinel.model2_type, "args": {}},
                "model 3": {"type": sentinel.model3_type, "args": {}},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
                "train": False,
                "test": True,
                "__RANDOM_SEED": 10,
            },
            sentinel.bootstrap_hash,
            {
                "epochs": sentinel.epochs,
                "verbose": sentinel.verbose,
                "batch size": sentinel.batch_size,
                "train": False,
                "test": True,
                "__DEBUG": sentinel.debug,
                "__RANDOM_SEED": 10,
                "BOOTSTRAP HASH": sentinel.bootstrap_hash,
                "model 1": {
                    "type": sentinel.model1,
                    "args": {"debug": sentinel.debug, "random_seed": 10},
                },
                "model 2": {
                    "type": sentinel.model2,
                    "args": {"debug": sentinel.debug, "random_seed": 11},
                },
                "model 3": {
                    "type": sentinel.model3,
                    "args": {"debug": sentinel.debug, "random_seed": 12},
                },
                "dataset": {
                    "type": sentinel.dataset_type,
                    "args": {
                        "dataset_arg_key": sentinel.dataset_arg_val,
                        "debug": sentinel.debug,
                        "random_seed": 13,
                    },
                },
            },
            {
                "optimizer": sentinel.optimizer,
                "criterion": sentinel.loss,
                "train_data": sentinel.train_data,
                "val_data": sentinel.validation_data,
                "test_data": sentinel.test_data,
                "train_loader": sentinel.train_loader,
                "validation_loader": sentinel.validation_loader,
                "test_loader": sentinel.test_loader,
                "epochs": sentinel.epochs,
                "batch_size": sentinel.batch_size,
                "device": 0,
                "logger": sentinel.logger,
                "verbose": sentinel.verbose,
            },
            False,
        ),
        (
            {
                "model 1": {"type": sentinel.model1_type},
                "model 2": {"type": sentinel.model2_type},
                "model 3": {"type": sentinel.model3_type},
                "dataset": {
                    "type": sentinel.data_type,
                    "args": {"dataset_arg_key": sentinel.dataset_arg_val},
                },
                "train": False,
                "test": False,
            },
            None,
            None,
            None,
            True,
        ),
    ],
)
def test_init_from_config_with_cuda(
    patch_custom_dataset,
    patch_data_loader,
    patch_get_opt_loss,
    patch_load_data,
    patch_logger,
    patch_model_from_config,
    patch_torch_cuda_is_available_true,
    config,
    bootstrap_hash,
    expected_output_config,
    expected_output,
    expected_error,
):
    processed_config = {
        "epochs": config.get("epochs", sentinel.epochs),
        "batch size": config.get("batch size", sentinel.batch_size),
        "verbose": config.get("verbose", sentinel.verbose),
        "__DEBUG": config.get("__DEBUG", sentinel.debug),
    }

    for k, v in config.items():
        processed_config[k] = v

    try:
        result = read_config_init.init_from_config(processed_config, bootstrap_hash)
        assert not expected_error
    except AssertionError as e:
        if not expected_error:
            raise e
        else:
            return

    result_config, result_kwarg = result

    del result_kwarg["model"]

    assert result_kwarg == expected_output
    assert result_config == expected_output_config


@pytest.mark.parametrize(
    "model_type, inputs, targets, random_seed, args, kwargs, expected, expected_kwargs, expected_error",
    [
        (
            "linear",
            torch.ones((10, 5)),
            torch.tensor([0, 1, 2, 3, 4, 5, 4, 3, 2, 1], dtype=torch.int32),
            10,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
            },
            (sentinel.FeedForward, "feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [5],
                "out_shape": [1],
                "in_features": 5,
                "num_classes": 6,
            },
            False,
        ),
        (
            "conv",
            torch.ones((2, 8, 8, 1)),
            torch.ones((2, 4, 4, 1)),
            10,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
            },
            (sentinel.ConvFeedForward, "conv-feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [8, 8, 1],
                "out_shape": [4, 4, 1],
                "num_classes": 1,
                "in_features": 64,
            },
            False,
        ),
        (
            "vqc-multi",
            torch.ones((10, 5)),
            None,
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.VQC, "vqc"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 5,
            },
            False,
        ),
        (
            "forward",
            torch.ones((10, 5)),
            None,
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.FeedForward, "feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 5,
            },
            False,
        ),
        (
            "forward",
            torch.ones((220, 2)),
            torch.arange(0, 220, dtype=torch.int32),
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.FeedForward, "feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 2,
                "num_classes": 1,
            },
            False,
        ),
        (
            "forward",
            torch.ones((220, 2)),
            torch.tensor([[x, 1, 2, 3] for x in range(220)], dtype=torch.int32),
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.FeedForward, "feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 2,
                "num_classes": 4,
            },
            False,
        ),
        (
            "forward",
            torch.ones((220, 2)),
            torch.tensor([[x, 1, 2, 3] for x in range(220)], dtype=torch.int32),
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.FeedForward, "feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 2,
                "num_classes": 4,
            },
            False,
        ),
        (
            "conv",
            torch.ones((220, 2)),
            torch.tensor([[[x, 1, 2, 3]] for x in range(220)], dtype=torch.int32),
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.ConvFeedForward, "conv-feed-forward"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 2,
                "num_classes": None,
            },
            False,
        ),
        (
            "single",
            torch.ones((220, 2)),
            torch.tensor([[x, 1, 2, 3] for x in range(220)], dtype=torch.int32),
            None,
            [sentinel.args],
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
            },
            (sentinel.SelectSingleOutput, "select-single-output"),
            {
                "kwarg_key_1": sentinel.kwarg_value_1,
                "kwarg_key_2": sentinel.kwarg_value_2,
                "in_shape": [3],
                "out_shape": [13, 43],
                "in_features": 2,
                "num_classes": 4,
            },
            False,
        ),
        ("invalid", torch.ones((10, 10)), None, None, [], {}, None, None, True),
    ],
)
def test_load_single_model(
    patch_model_vqc,
    patch_model_conv_feed_forward,
    patch_model_feed_forward,
    patch_model_select_single_output,
    patch_np_random,
    patch_torch_random,
    patch_random,
    model_type,
    inputs,
    targets,
    random_seed,
    args,
    kwargs,
    expected,
    expected_kwargs,
    expected_error,
):
    try:
        result = read_config_init._load_single_model(
            model_type, inputs, targets, random_seed, *args, **kwargs,
        )

        assert not expected_error

        assert result == expected

        if random_seed is not None:

            assert patch_np_random[1].mock_calls == [call(random_seed)]
            assert patch_np_random[2].mock_calls == [call(sentinel.np_random_state)]
            assert patch_torch_random[1].mock_calls == [call(random_seed)]
            assert patch_torch_random[2].mock_calls == [
                call(sentinel.torch_random_state)
            ]
            assert patch_random[1].mock_calls == [call(random_seed)]
            assert patch_random[2].mock_calls == [call(sentinel.random_state)]
        model_type_clean = result[1].replace("-", "_")
        assert locals()[f"patch_model_{model_type_clean}"].mock_calls == [
            call(*args, **expected_kwargs),
        ]

    except ValueError as e:
        if not expected_error:
            raise e


@pytest.mark.parametrize(
    "config, inputs, expected_single_model_calls, expected_warning_calls, expected_torch_load_calls, expected_model_load_state_dict_calls, raise_exception_load_state_dict",
    [
        (
            {
                "model 1": {
                    "type": "SimpleMockClassicalModel",
                    "args": {"sentinel_key": sentinel.value1},
                },
                "model 2": {
                    "type": "SimpleMockQuantumModel",
                    "args": {"sentinel_key": sentinel.value2},
                },
                "model 3": {
                    "type": "SimpleMockClassicalModel",
                    "args": {"sentinel_key": sentinel.value3},
                },
                "model 2 3 func": lambda x: 31 * x,
            },
            np.ones((10, 5)),
            [
                call(
                    "SimpleMockClassicalModel",
                    torch.ones((10, 5)),
                    None,
                    sentinel_key=sentinel.value1,
                ),
                call(
                    "SimpleMockQuantumModel",
                    torch.ones((10, 5)),
                    None,
                    sentinel_key=sentinel.value2,
                ),
                call(
                    "SimpleMockClassicalModel",
                    0.5 * torch.ones((10, 5)),
                    sentinel.targets,
                    sentinel_key=sentinel.value3,
                ),
            ],
            [],
            [],
            [],
            False,
        ),
        (
            {
                "model 1": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value1},
                },
                "model 2": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value2},
                    "load from": sentinel.load_file2,
                },
                "model 3": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value3},
                    "load from": sentinel.load_file3,
                },
            },
            np.ones((2, 3, 3, 1)),
            [
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value1,
                ),
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value2,
                ),
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    sentinel.targets,
                    sentinel_key=sentinel.value3,
                ),
            ],
            [],
            [call(sentinel.load_file2), call(sentinel.load_file3)],
            [
                call(
                    {
                        "layer1": sentinel.layer21,
                        "layer2": sentinel.layer22,
                        "layer3": sentinel.layer23,
                    }
                ),
                call(
                    {
                        "layer1": sentinel.layer31,
                        "layer2": sentinel.layer32,
                        "layer3": sentinel.layer33,
                    }
                ),
            ],
            False,
        ),
        (
            {
                "model 1": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value1},
                    "load from": sentinel.load_file1,
                },
                "model 2": {
                    "type": "simple_mock_quantum_model",
                    "args": {"sentinel_key": sentinel.value2},
                },
                "model 3": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value3},
                },
            },
            np.ones((2, 3, 3, 1)),
            [
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value1,
                ),
                call(
                    "simple_mock_quantum_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value2,
                ),
                call(
                    "simple_mock_classical_model",
                    0.5 * torch.ones((2, 3, 3, 1)),
                    sentinel.targets,
                    sentinel_key=sentinel.value3,
                ),
            ],
            [],
            [call(sentinel.load_file1)],
            [
                call(
                    {
                        "layer1": sentinel.layer11,
                        "layer2": sentinel.layer12,
                        "layer3": sentinel.layer13,
                    }
                ),
            ],
            False,
        ),
        (
            {
                "model 1": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value1},
                },
                "model 2": {
                    "type": "simple_mock_quantum_model",
                    "args": {"sentinel_key": sentinel.value2},
                    "load from": sentinel.load_file2,
                },
                "model 3": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value3},
                },
            },
            np.ones((2, 3, 3, 1)),
            [
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value1,
                ),
                call(
                    "simple_mock_quantum_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value2,
                ),
                call(
                    "simple_mock_classical_model",
                    0.5 * torch.ones((2, 3, 3, 1)),
                    sentinel.targets,
                    sentinel_key=sentinel.value3,
                ),
            ],
            [
                call(
                    "Model 2 is not a torch Module, and hence it cannot be loaded from a different file."
                ),
            ],
            [],
            [],
            False,
        ),
        (
            {
                "model 1": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value1},
                    "load from": sentinel.load_file1,
                },
                "model 2": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value2},
                    "load from": sentinel.load_file2,
                },
                "model 3": {
                    "type": "simple_mock_classical_model",
                    "args": {"sentinel_key": sentinel.value3},
                    "load from": sentinel.load_file3,
                },
            },
            np.ones((2, 3, 3, 1)),
            [
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value1,
                ),
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    None,
                    sentinel_key=sentinel.value2,
                ),
                call(
                    "simple_mock_classical_model",
                    torch.ones((2, 3, 3, 1)),
                    sentinel.targets,
                    sentinel_key=sentinel.value3,
                ),
            ],
            [
                call("Failed to load state dict for model 1 with exception:\n"),
                call("Failed to load state dict for model 2 with exception:\n"),
                call("Failed to load state dict for model 3 with exception:\n"),
            ],
            [
                call(sentinel.load_file1),
                call(sentinel.load_file2),
                call(sentinel.load_file3),
            ],
            [
                call(
                    {
                        "layer1": sentinel.layer11,
                        "layer2": sentinel.layer12,
                        "layer3": sentinel.layer13,
                    }
                ),
                call(
                    {
                        "layer1": sentinel.layer21,
                        "layer2": sentinel.layer22,
                        "layer3": sentinel.layer23,
                    }
                ),
                call(
                    {
                        "layer1": sentinel.layer31,
                        "layer2": sentinel.layer32,
                        "layer3": sentinel.layer33,
                    }
                ),
            ],
            True,
        ),
    ],
)
def test_model_from_config(
    patch_warnings_warn,
    patch_torch_load,
    simple_mock_classical_model_subclass,
    simple_mock_quantum_model_subclass,
    config,
    inputs,
    expected_single_model_calls,
    expected_warning_calls,
    expected_torch_load_calls,
    expected_model_load_state_dict_calls,
    raise_exception_load_state_dict,
):
    SimpleMockClassicalModel.load_state_dict = Mock(side_effect=lambda x: x)

    patch_single_model = read_config_init._load_single_model = Mock(
        side_effect=lambda x, *args, **kwargs: (get_model(x), x),
    )

    if raise_exception_load_state_dict:
        SimpleMockQuantumModel.load_state_dict = (
            SimpleMockClassicalModel.load_state_dict
        ) = Mock(side_effect=lambda *args, **kwargs: raise_value_error())

    result = read_config_init.model_from_config(config, inputs, sentinel.targets)

    assert len(patch_single_model.mock_calls) == len(expected_single_model_calls)
    for patch_call, expected_call in zip(
        patch_single_model.mock_calls, expected_single_model_calls
    ):
        for patch_part, expected_part in zip(patch_call, expected_call):
            try:
                assert patch_part == expected_part
            except RuntimeError:
                for patch_arg, expected_arg in zip(patch_part, expected_part):
                    assert isinstance(patch_arg, type(expected_arg))
                    if isinstance(patch_arg, torch.Tensor):
                        assert torch.all(patch_arg == expected_arg)
                    else:
                        assert patch_arg == expected_arg

    assert patch_warnings_warn.mock_calls == expected_warning_calls
    assert patch_torch_load.mock_calls == expected_torch_load_calls

    # load_state_dict became a static method
    assert (
        result.model1.load_state_dict.mock_calls == expected_model_load_state_dict_calls
    )
