from unittest.mock import Mock, call, sentinel

import pytest

from squid_helpers.config_input import get_loss_opt


@pytest.fixture
def patch_path():
    return "squid_helpers.config_input.get_loss_opt"


@pytest.fixture
def patch_nn(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.nn",
        Mock(
            MSELoss=Mock(return_value=sentinel.mse),
            CrossEntropyLoss=Mock(return_value=sentinel.ce),
            NLLLoss=Mock(return_value=sentinel.nll),
            module=Mock(loss=Mock(_Loss=sentinel.loss)),
        ),
    )


@pytest.fixture
def patch_optim(mocker, patch_path, mock_adam_optimizer, mock_sgd_optimizer):
    return mocker.patch(
        f"{patch_path}.optim", Mock(Adam=mock_adam_optimizer, SGD=mock_sgd_optimizer,)
    )


@pytest.fixture
def patch_inspect(mocker, patch_path):
    return mocker.patch(
        f"{patch_path}.inspect",
        Mock(
            signature=Mock(
                return_value=Mock(
                    parameters=Mock(
                        keys=Mock(
                            return_value={
                                "optimizer_arg_1": sentinel.arg,
                                "optimizer_arg_2": sentinel.arg,
                            }
                        )
                    )
                )
            )
        ),
    )


@pytest.fixture
def mock_adam_optimizer():
    return Mock(return_value=sentinel.adam)


@pytest.fixture
def mock_sgd_optimizer():
    return Mock(return_value=sentinel.sgd)


@pytest.mark.parametrize(
    "config, model_parameters, expected_output, expected_error, expected_optimizer_calls",
    [
        (
            {
                "optimizer": {
                    "type": "adam",
                    "args": {
                        "optimizer_arg_1": sentinel.opt_arg_1,
                        "optimizer_arg_2": sentinel.opt_arg_2,
                    },
                },
                "loss": {"type": "mse"},
            },
            sentinel.model_parameters,
            (sentinel.adam, "adam", sentinel.mse, "mse"),
            False,
            [
                call(
                    sentinel.model_parameters,
                    optimizer_arg_1=sentinel.opt_arg_1,
                    optimizer_arg_2=sentinel.opt_arg_2,
                )
            ],
        ),
        (
            {
                "optimizer": {
                    "type": "sgd",
                    "args": {
                        "optimizer_arg_1": sentinel.opt_arg_1,
                        "optimizer_arg_3": sentinel.opt_arg_3,
                    },
                },
                "loss": {"type": "nll"},
            },
            sentinel.model_parameters,
            (sentinel.sgd, "sgd", sentinel.nll, "nll"),
            False,
            [call(sentinel.model_parameters, optimizer_arg_1=sentinel.opt_arg_1)],
        ),
        (
            {
                "optimizer": {"type": "ADAM", "args": {}},
                "loss": {"type": "cross entropy"},
            },
            sentinel.model_parameters,
            (sentinel.adam, "adam", sentinel.ce, "ce"),
            False,
            [call(sentinel.model_parameters)],
        ),
        (
            {
                "optimizer": {"type": "SGD", "args": {}},
                "loss": {"type": "not a valid loss"},
            },
            sentinel.model_parameters,
            None,
            True,
            None,
        ),
        (
            {
                "optimizer": {"type": "not a valid optimizer", "args": {}},
                "loss": {"type": "ce"},
            },
            sentinel.model_parameters,
            None,
            True,
            None,
        ),
    ],
)
def test_get_loss_opt(
    patch_inspect,
    patch_nn,
    patch_optim,
    mock_adam_optimizer,
    mock_sgd_optimizer,
    config,
    model_parameters,
    expected_output,
    expected_error,
    expected_optimizer_calls,
):
    model = Mock(parameters=Mock(return_value=model_parameters))

    try:
        output = get_loss_opt.get_loss_opt(config, model)

        assert not expected_error
    except ValueError as e:
        if expected_error:
            return
        else:
            raise e

    assert output == expected_output
    if output[1] == "adam":  # output[1] is optimizer_type:
        assert mock_adam_optimizer.mock_calls == expected_optimizer_calls
    elif output[1] == "sgd":
        assert mock_sgd_optimizer.mock_calls == expected_optimizer_calls
    else:
        raise ValueError("Is there a new optimizer added? Please create mock for it.")
