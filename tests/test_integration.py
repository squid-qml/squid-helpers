import glob
import subprocess
from logging import Logger

import pytest

LOGGER = Logger(__name__)

TEST_CONFIGS = list(glob.glob("tests/integration_test_configs/*.yml"))


@pytest.mark.integration
@pytest.mark.parametrize("test_file", TEST_CONFIGS)
def test_configs(test_file):
    print(f"Executing Command: python main.py {test_file}")
    output = subprocess.run(["python", "main.py", test_file])
    assert output.returncode == 0
