import invoke
import shlex


TEST_COVERAGE_FAIL = 85


def __read_meta_yaml(ctx):
    try:
        import yaml
    except:
        ctx.run("pip install pyyaml")

    import yaml

    with open("meta.yaml", mode="r") as f:
        packages = yaml.safe_load(f)
    return packages


@invoke.task
def install(ctx):
    """Installs squidpy-helpers"""
    ctx.run("python -m pip install .")


@invoke.task
def develop(ctx, pip=False):
    """Install squidpy-helpers in development mode."""
    packages = __read_meta_yaml(ctx)["requirements"]["develop"]

    secure_packages = [shlex.quote(x) for x in packages]
    if not secure_packages:
        return
    if pip:
        secure_packages = [x.replace(" ", "==") for x in secure_packages]
        ctx.run("pip install " + " ".join(secure_packages), echo=True)
    else:
        ctx.run("conda install --yes " + " ".join(secure_packages), echo=True)


@invoke.task
def install_required_packages(ctx, pip=False):
    packages = __read_meta_yaml(ctx)["requirements"]

    secure_packages = [shlex.quote(x) for x in packages["run"]]
    if pip:
        secure_packages = [x.replace(" ", "==") for x in secure_packages]
        ctx.run("pip install " + " ".join(secure_packages), echo=True)
    else:
        ctx.run("conda install --yes " + " ".join(secure_packages), echo=True)

    secure_packages = [shlex.quote(x) for x in packages["torch"]]
    if pip:
        secure_packages = ["torch" if x.startswith("pytorch") else x for x in secure_packages]
        secure_packages = [x.replace(" ", "==") for x in secure_packages if not x.startswith("cudatoolkit")]
        ctx.run("pip install " + " ".join(secure_packages), echo=True)
    else:
        ctx.run("conda install --yes " + " ".join(secure_packages) + " -c pytorch", echo=True)

    secure_packages = [shlex.quote(x) for x in packages["pypi"]]
    secure_packages = [x.replace(" ", "==") for x in secure_packages]
    ctx.run("pip install " + " ".join(secure_packages), echo=True)

# Maybe hooks for bitbucket?


@invoke.task
def docs(ctx):
    # Basically need to decided whether to use something simple like pdoc3, or fancy like Sphinx?
    pass


@invoke.task(optional=["apply"])
def lint(ctx, apply=False):
    ctx.run("flake8 squid_helpers tests", echo=True)

    isort_flags = ["--diff", "--check-only"]
    black_flags = ["--diff"]

    if apply:
        isort_flags = black_flags = []

    isort_flags = " ".join(isort_flags)
    black_flags = " ".join(black_flags)

    ctx.run(
        f"isort {isort_flags} squid_helpers tests", echo=True
    )
    result = ctx.run(
        f"black {black_flags} squid_helpers tests", echo=True
    )
    if "reformatted" not in result.stderr:
        ctx.run("mypy --no-incremental --cache-dir /dev/null squid_helpers", echo=True)


@invoke.task(
    help={
        "integration": "Whether to run integration tests or not. (Default: False)"
    },
)
def test(ctx, integration=False):
    if integration:
        flags = "-m integration"
    else:
        flags = f"--cov=squid_helpers --cov-fail-under={TEST_COVERAGE_FAIL} -m \"not integration\""

    ctx.run("pytest " + flags, echo=True)
