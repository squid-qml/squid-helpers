import random
from pathlib import Path
from typing import Any, Callable, Dict, Optional

import torch
import yaml

try:
    # Required for running from command
    from squid_helpers.config_input.read_config_init import (  # type: ignore
        init_from_config,
        init_from_config_path,
        read_configs,
    )
except ModuleNotFoundError:
    # Required for testing
    from squid_helpers.config_input.read_config_init import (
        init_from_config,
        init_from_config_path,
        read_configs,
    )
# Same as above
try:
    from squid_helpers.loggers import BootstrapLogger, Logger
except ModuleNotFoundError:
    from squid_helpers.loggers import BootstrapLogger, Logger

from joblib import Parallel, delayed
from torch.utils.data import DataLoader
from tqdm import tqdm


def __get_func_only_args(func: Callable, kwargs: Dict[str, Any]) -> Dict[str, Any]:
    required_arguments = set(func.__code__.co_varnames[: func.__code__.co_argcount])
    optional_arguments = set(
        func.__code__.co_varnames[
            func.__code__.co_argcount : func.__code__.co_argcount
            + func.__code__.co_kwonlyargcount
        ]
    )
    assert set.issubset(
        required_arguments, kwargs.keys()
    ), f"Missing arguments with keys: {required_arguments.difference(kwargs.keys())} in kwargs"
    processed_kwargs = {
        key: val
        for key, val in kwargs.items()
        if key in optional_arguments.union(required_arguments)
    }
    return processed_kwargs


def main():
    config_paths = read_configs()
    for config_path in config_paths:
        config, kwargs = init_from_config_path(config_path)

        if "BOOTSTRAP" in config and config["BOOTSTRAP"]:
            bootstrap(config_path, config)
        else:
            if config["train"]:
                train(**__get_func_only_args(train, kwargs))
            if config["test"]:
                test(**__get_func_only_args(test, kwargs))


# Easy call to single run of bootstrap
def _single_run(config, bootstrap_hash, bootstrap_idx: int = None):
    if isinstance(config, (str, Path)):
        with open(config, mode="r") as config_file:
            config = yaml.safe_load(config_file)

    if (
        bootstrap_idx is not None
        and "__RANDOM_SEED" in config
        and config["__RANDOM_SEED"] is not None
    ):
        config["__RANDOM_SEED"] += bootstrap_idx

    if bootstrap_idx is not None and config["log folder"] is not None:
        config["log folder"] = str(
            Path(config["log folder"]) / "log" / f"{bootstrap_idx}"
        )

    config, kwargs = init_from_config(config, bootstrap_hash=bootstrap_hash)

    assert (
        "logger" in kwargs
    ), "Returning logger is required in kwargs from init_from_config"

    if config["train"]:
        train(**__get_func_only_args(train, kwargs))
    if config["test"]:
        test(**__get_func_only_args(test, kwargs))

    # Logger contains all info we need
    return kwargs["logger"]


def bootstrap(config_path, config):
    # Generate and add hash to config
    bootstrap_hash = random.randint(0, 100000000)

    # Initialize bootstrap logger
    bootstrap_logger = BootstrapLogger(config, bootstrap_hash)

    # Run many experiments
    if "BOOTSTRAP RUNS" not in config:
        raise ValueError(
            "BOOTSTRAP RUNS key needed in config. Value needs to be an int."
        )

    if "BOOTSTRAP JOBS" in config:
        with open(config_path, mode="r") as config_file:
            config = yaml.safe_load(config_file)
        parallel_results = Parallel(n_jobs=config["BOOTSTRAP JOBS"])(
            delayed(_single_run)(config_path, bootstrap_hash, idx)
            for idx in range(config["BOOTSTRAP RUNS"])
        )
        for result in parallel_results:
            bootstrap_logger.process_iter(result)
    else:
        for idx in range(config["BOOTSTRAP RUNS"]):
            bootstrap_logger.process_iter(_single_run(config_path, bootstrap_hash, idx))

    bootstrap_logger.process_end()


def train(
    model,
    optimizer,
    criterion,
    device,
    train_loader: DataLoader,
    *,
    validation_loader: DataLoader = None,
    epochs: int = 1,
    logger: Optional[Logger] = None,
    verbose: int = 1,
):
    # Send model to device
    model = model.to(device)

    # Train Model
    for epoch_idx in range(epochs):
        print(f"Epoch: {epoch_idx}")

        # Keep track of loss
        running_loss = 0.0

        # Track y arrays for epoch (need to track truth in case loader is shuffled)
        y_pred = []
        y_truth = []

        for inputs, labels in tqdm(
            train_loader, desc="Batch", leave=False, disable=(verbose == 0)
        ):
            # Zero the parameter gradients
            optimizer.zero_grad()

            # Send to device
            inputs = inputs.to(device)

            # Squeeze (view(-1)) makes sure than shape of labels matches one required by criterion.
            # Again just being extra careful. Redundant with reshape at the begining of function.
            # * Note that the view(-1) changes shape of [1, 1] tensor to [1], instead of []
            # * like squeeze_() which is preferred in this case
            labels = labels.view(-1).to(device)

            # Forward
            pred = model(inputs)

            # Backward
            # Decoder loss + updates of its parameters
            loss = criterion(pred, labels)

            # Update gradients of parameters of the model.
            # Note that there is `model.backward()` call. THIS IS NEEDED!
            # Also, the only difference from the standard PyTorch train loop
            loss.backward()
            model.backward()

            # Update parameters, based on gradient and opt algorithm.
            optimizer.step()

            # Add prediction and truth tracking for logger to use later.
            y_pred.extend(torch.argmax(pred, dim=1).detach().cpu().tolist())
            y_truth.extend(labels.detach().cpu().tolist())

            # Update loss tracking
            running_loss += loss.item()

            # Call logger at end of update
            if logger is not None:
                logger.process_update(running_loss, validation_loader)

        # Normalize the loss
        running_loss /= len(train_loader)

        # Call logger at end of epoch
        if logger is not None:
            logger.process_iter(y_truth, y_pred, running_loss, validation_loader)
        print("")

    if logger is not None:
        logger.process_end()


def test(
    model,
    criterion,
    device,
    test_loader: DataLoader,
    *,
    logger: Optional[Logger] = None,
    verbose: int = 1,
):
    # Send model to device
    model = model.to(device)

    # Keep track of loss
    running_loss = 0.0

    # Track y arrays for epoch (need to track truth in case loader is shuffled)
    y_pred = []
    y_truth = []

    with torch.no_grad:  # type: ignore
        for inputs, labels in tqdm(
            test_loader, desc="Batch", leave=False, disable=(verbose == 0)
        ):
            # Send to device
            inputs = inputs.to(device)

            # Squeeze (view(-1)) makes sure than shape of labels matches one required by criterion.
            # Again just being extra careful. Redundant with reshape at the begining of function.
            # * Note that the view(-1) changes shape of [1, 1] tensor to [1], instead of []
            # * like squeeze_() which is preferred in this case
            labels = labels.view(-1).to(device)

            # Forward
            pred = model(inputs)

            # Backward
            # Decoder loss + updates of its parameters
            loss = criterion(pred, labels)

            # Add prediction and truth tracking for logger to use later.
            y_pred.extend(torch.argmax(pred, dim=1).detach().cpu().tolist())
            y_truth.extend(labels.detach().cpu().tolist())

            # Update loss tracking
            running_loss += loss.item()

        # Normalize the loss
        running_loss /= len(test_loader)

        if logger is not None:
            logger.process_test(y_truth, y_pred, running_loss)


def init():
    if __name__ == "__main__":
        main()


init()
